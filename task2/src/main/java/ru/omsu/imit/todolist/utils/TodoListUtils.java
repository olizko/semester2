package ru.omsu.imit.todolist.utils;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.lang.StringUtils;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import ru.omsu.imit.todolist.exception.TodoListException;
import ru.omsu.imit.todolist.rest.response.FailureResponse;

public class TodoListUtils {

    private static final Gson GSON = new Gson();

    public static <T> T getClassInstanceFromJson(Gson gson, String json, Class<T> clazz) throws TodoListException {
        if (StringUtils.isEmpty(json)) {
            throw new TodoListException(ErrorCode.NULL_REQUEST);
        }
        try {
            return gson.fromJson(json, clazz);
        } catch (JsonSyntaxException ex) {
            throw new TodoListException(ErrorCode.JSON_PARSE_EXCEPTION, json);
        }
    }

    public static Response failureResponse(Status status, TodoListException ex) {
        return Response.status(status).entity(GSON.toJson(new FailureResponse(ex.getErrorCode(), ex.getMessage()))).build();
    }

    public static Response failureResponse(TodoListException ex) {
        return failureResponse(Status.BAD_REQUEST, ex);
    }

}
