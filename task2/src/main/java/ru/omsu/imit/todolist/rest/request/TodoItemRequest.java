package ru.omsu.imit.todolist.rest.request;

public class TodoItemRequest {

    private String text;
    private String userFirstName;
    private String userLastName;

    TodoItemRequest(){
    }

    public TodoItemRequest(String text, String userFirstName, String userLastName){
        this.text = text;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
    }

    public TodoItemRequest(String text){
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }
}