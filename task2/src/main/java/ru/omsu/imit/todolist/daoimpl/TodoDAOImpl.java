package ru.omsu.imit.todolist.daoimpl;

import ru.omsu.imit.todolist.dao.TodoDAO;
import ru.omsu.imit.todolist.database.DataBaseEmulator;
import ru.omsu.imit.todolist.exception.TodoListException;
import ru.omsu.imit.todolist.model.TodoItem;
import ru.omsu.imit.todolist.utils.ErrorCode;
import java.util.List;

public class TodoDAOImpl implements TodoDAO{

    public TodoItem addItem(TodoItem item){
        DataBaseEmulator.addItem(item);
        return item;
    }

    public TodoItem getById(int id) throws TodoListException{
        TodoItem item = DataBaseEmulator.getById(id);
        if (item == null)
            throw new TodoListException(ErrorCode.ITEM_NOT_FOUND, Integer.toString(id));
        return item;
    }

    public List<TodoItem> getAll() {
        return DataBaseEmulator.getAll();
    }

    public TodoItem editItem(int id, String newText) throws TodoListException{
        if(!DataBaseEmulator.editItemById(id, newText))
            throw new TodoListException(ErrorCode.ITEM_NOT_FOUND, Integer.toString(id));
        return new TodoItem(id, newText);
    }

    public void deleteById(int id) throws TodoListException{
        if(!DataBaseEmulator.deleteById(id))
            throw new TodoListException(ErrorCode.ITEM_NOT_FOUND, Integer.toString(id));
    }

}