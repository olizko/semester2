package ru.omsu.imit.todolist.resources;

import ru.omsu.imit.todolist.rest.request.*;
import ru.omsu.imit.todolist.rest.response.*;
import com.google.gson.Gson;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api")
public class TodoListResource {

    private static final Gson GSON = new Gson();

    @POST
    @Path("/todolist")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addTodoItem(String json) {
        String response = GSON.toJson(new TodoItemInfoResponse(1, "Сдать экзамен"));
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/todolist/{id}")
    @Produces("application/json")
    public Response getById(@PathParam(value = "id") int id) {
        String response = GSON.toJson(new TodoItemInfoResponse(1, "Сдать экзамен"));
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    @PUT
    @Path("/todolist/{id}")
    @Produces("application/json")
    public Response editById(@PathParam(value = "id") int id, String json) {
        String response = GSON.toJson(new TodoItemInfoResponse(1, "Пересдать экзамен"));
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    @DELETE
    @Path("/todolist/{id}")
    @Produces("application/json")
    public Response deleteById(@PathParam(value = "id") int id, String json) {
        String response = GSON.toJson(new EmptySuccessResponse());
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

}
