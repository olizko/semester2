package ru.omsu.imit.todolist.model;

public class TodoItem {

    private int id;
    private String text;

    public TodoItem(int id, String text){
        this.id = id;
        this.text = text;
    }

    public TodoItem(String text){
        this.text = text;
    }

    public TodoItem(){

    }

    public String getText(){
        return this.text;
    }

    public int getId(){
        return this.id;
    }

    public void setText(String text){
        this.text = text;
    }

    public void setId(int id){
        this.id = id;
    }

    @Override
    public int hashCode(){
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        result = prime * result + ((text == null) ? 0 : text.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object o){

        if(this == o) return true;
        if(o == null) return false;
        if(getClass() != o.getClass()) return false;

        TodoItem other = (TodoItem)o;
        if (this.id != other.id) return false;
        if (text == null){
            if (other.text != null) return false;
        }else if (!text.equals(other.text)) return false;

        return true;

    }

}