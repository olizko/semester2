package ru.omsu.imit.rest.response.plane;

public class EditPlaneResponse {
    private String name;
    private String capacity;

    public EditPlaneResponse(String name, String capacity) {
        this.name = name;
        this.capacity = capacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EditPlaneResponse that = (EditPlaneResponse) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return capacity != null ? capacity.equals(that.capacity) : that.capacity == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (capacity != null ? capacity.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EditPlaneResponse{" +
                "name='" + name + '\'' +
                ", capacity='" + capacity + '\'' +
                '}';
    }
}
