package ru.omsu.imit.rest.response.plane;


public class GetPlaneResponse {
    private int planeId;
    private String name;
    private String capacity;

    public GetPlaneResponse(int planeId, String name, String capacity) {
        this.planeId = planeId;
        this.name = name;
        this.capacity = capacity;
    }

    public int getPlaneId() {
        return planeId;
    }

    public String getName() {
        return name;
    }

    public String getCapacity() {
        return capacity;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GetPlaneResponse that = (GetPlaneResponse) o;

        if (planeId != that.planeId) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return capacity != null ? capacity.equals(that.capacity) : that.capacity == null;
    }

    @Override
    public int hashCode() {
        int result = planeId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (capacity != null ? capacity.hashCode() : 0);
        return result;
    }
}
