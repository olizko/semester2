package ru.omsu.imit.rest.response.plane;

public class PlaneResponse {
    private int planeId;
    private String name;
    private String capacity;

    public PlaneResponse(int planeId, String name, String capacity) {
        this.planeId = planeId;
        this.name = name;
        this.capacity = capacity;
    }

    public String getName() {
        return name;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public int getPlaneId() {
        return planeId;
    }

    public void setPlaneId(int planeId) {
        this.planeId = planeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PlaneResponse that = (PlaneResponse) o;

        if (planeId != that.planeId) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return capacity != null ? capacity.equals(that.capacity) : that.capacity == null;
    }

    @Override
    public int hashCode() {
        int result = planeId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (capacity != null ? capacity.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PlaneResponse{" +
                "planeId=" + planeId +
                ", name='" + name + '\'' +
                ", capacity='" + capacity + '\'' +
                '}';
    }
}

