package ru.omsu.imit.rest.request.flight;

import ru.omsu.imit.model.Plane;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

public class AddFlightRequest {
    private String flightFrom;
    private String flightTo;
    private String date;
    private String timeArrival;
    private String timeDepartment;

    public AddFlightRequest(String flightFrom, String flightTo, String date, String timeArrival, String timeDepartment) {
        this.flightFrom = flightFrom;
        this.flightTo = flightTo;
        this.date = date;
        this.timeArrival = timeArrival;
        this.timeDepartment = timeDepartment;
    }


    public String getFlightFrom() {
        return flightFrom;
    }

    public void setFlightFrom(String flightFrom) {
        this.flightFrom = flightFrom;
    }

    public String getFlightTo() {
        return flightTo;
    }

    public void setFlightTo(String flightTo) {
        this.flightTo = flightTo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimeArrival() {
        return timeArrival;
    }

    public void setTimeArrival(String timeArrival) {
        this.timeArrival = timeArrival;
    }

    public String getTimeDepartment() {
        return timeDepartment;
    }

    public void setTimeDepartment(String timeDepartment) {
        this.timeDepartment = timeDepartment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AddFlightRequest that = (AddFlightRequest) o;

        if (flightFrom != null ? !flightFrom.equals(that.flightFrom) : that.flightFrom != null) return false;
        if (flightTo != null ? !flightTo.equals(that.flightTo) : that.flightTo != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (timeArrival != null ? !timeArrival.equals(that.timeArrival) : that.timeArrival != null) return false;
        return timeDepartment != null ? timeDepartment.equals(that.timeDepartment) : that.timeDepartment == null;
    }

    @Override
    public int hashCode() {
        int result = flightFrom != null ? flightFrom.hashCode() : 0;
        result = 31 * result + (flightTo != null ? flightTo.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (timeArrival != null ? timeArrival.hashCode() : 0);
        result = 31 * result + (timeDepartment != null ? timeDepartment.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AddFlightRequest{" +
                "flightFrom='" + flightFrom + '\'' +
                ", flightTo='" + flightTo + '\'' +
                ", date=" + date +
                ", timeArrival=" + timeArrival +
                ", timeDepartment=" + timeDepartment +
                '}';
    }
}
