package ru.omsu.imit.rest.request.passenger;

import java.util.Objects;

public class AddPassengerToTicketRequest {
    private int seat;
    private String clazz;
    private int passengerId;
    private int flightId;

    public AddPassengerToTicketRequest(int seat, String clazz, int passengerId, int flightId) {
        this.seat = seat;
        this.clazz = clazz;
        this.passengerId = passengerId;
        this.flightId = flightId;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public int getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(int passengerId) {
        this.passengerId = passengerId;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AddPassengerToTicketRequest that = (AddPassengerToTicketRequest) o;

        if (seat != that.seat) return false;
        if (passengerId != that.passengerId) return false;
        if (flightId != that.flightId) return false;
        return clazz != null ? clazz.equals(that.clazz) : that.clazz == null;
    }

    @Override
    public int hashCode() {
        int result = seat;
        result = 31 * result + (clazz != null ? clazz.hashCode() : 0);
        result = 31 * result + passengerId;
        result = 31 * result + flightId;
        return result;
    }

    @Override
    public String toString() {
        return "AddPassengerToTicketRequest{" +
                "seat=" + seat +
                ", clazz='" + clazz + '\'' +
                ", passengerId=" + passengerId +
                ", flightId=" + flightId +
                '}';
    }
}
