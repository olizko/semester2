package ru.omsu.imit.rest.response.flight;

import ru.omsu.imit.model.Plane;
import ru.omsu.imit.rest.response.plane.PlaneResponse;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

public class AddFlightResponse {
    private int flightId;
    private String flightFrom;
    private String flightTo;
    private String date;
    private String timeArrival;
    private String timeDepartment;
    private int planeId;

    public AddFlightResponse(int flightId, String flightFrom, String flightTo, String date, String timeArrival, String timeDepartment, int planeId) {
        this.flightId = flightId;
        this.flightFrom = flightFrom;
        this.flightTo = flightTo;
        this.date = date;
        this.timeArrival = timeArrival;
        this.timeDepartment = timeDepartment;
        this.planeId = planeId;
    }

    public AddFlightResponse(String flightFrom, String flightTo, String date, String timeArrival, String timeDepartment, int planeId) {

        this.flightFrom = flightFrom;
        this.flightTo = flightTo;
        this.date = date;
        this.timeArrival = timeArrival;
        this.timeDepartment = timeDepartment;
        this.planeId = planeId;
    }

    public int getFlightId() {
        return flightId;
    }

    public String getFlightFrom() {
        return flightFrom;
    }

    public String getFlightTo() {
        return flightTo;
    }

    public String getDate() {
        return date;
    }

    public String getTimeArrival() {
        return timeArrival;
    }

    public String getTimeDepartment() {
        return timeDepartment;
    }

    public int getPlaneId() {
        return planeId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AddFlightResponse that = (AddFlightResponse) o;

        if (flightId != that.flightId) return false;
        if (planeId != that.planeId) return false;
        if (flightFrom != null ? !flightFrom.equals(that.flightFrom) : that.flightFrom != null) return false;
        if (flightTo != null ? !flightTo.equals(that.flightTo) : that.flightTo != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (timeArrival != null ? !timeArrival.equals(that.timeArrival) : that.timeArrival != null) return false;
        return timeDepartment != null ? timeDepartment.equals(that.timeDepartment) : that.timeDepartment == null;
    }

    @Override
    public int hashCode() {
        int result = flightId;
        result = 31 * result + (flightFrom != null ? flightFrom.hashCode() : 0);
        result = 31 * result + (flightTo != null ? flightTo.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (timeArrival != null ? timeArrival.hashCode() : 0);
        result = 31 * result + (timeDepartment != null ? timeDepartment.hashCode() : 0);
        result = 31 * result + planeId;
        return result;
    }

    @Override
    public String toString() {
        return "AddFlightResponse{" +
                "flightId=" + flightId +
                ", flightFrom='" + flightFrom + '\'' +
                ", flightTo='" + flightTo + '\'' +
                ", date='" + date + '\'' +
                ", timeArrival='" + timeArrival + '\'' +
                ", timeDepartment='" + timeDepartment + '\'' +
                ", planeId=" + planeId +
                '}';
    }
}