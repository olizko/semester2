package ru.omsu.imit.rest.response.passenger;

import ru.omsu.imit.rest.response.flight.GetFlightResponse;

public class AddPassengerToFlightResponse {
    private int seat;
    private String clazz;
    private PassengerResponse passengerResponse;
    private GetFlightResponse flightResponse;

    public AddPassengerToFlightResponse(int seat, String clazz, PassengerResponse passengerResponse, GetFlightResponse flightResponse) {
        this.seat = seat;
        this.clazz = clazz;
        this.passengerResponse = passengerResponse;
        this.flightResponse = flightResponse;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public PassengerResponse getPassengerResponse() {
        return passengerResponse;
    }

    public void setPassengerResponse(PassengerResponse passengerResponse) {
        this.passengerResponse = passengerResponse;
    }

    public GetFlightResponse getFlightResponse() {
        return flightResponse;
    }

    public void setFlightResponse(GetFlightResponse flightResponse) {
        this.flightResponse = flightResponse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AddPassengerToFlightResponse that = (AddPassengerToFlightResponse) o;

        if (seat != that.seat) return false;
        if (clazz != null ? !clazz.equals(that.clazz) : that.clazz != null) return false;
        if (passengerResponse != null ? !passengerResponse.equals(that.passengerResponse) : that.passengerResponse != null)
            return false;
        return flightResponse != null ? flightResponse.equals(that.flightResponse) : that.flightResponse == null;
    }

    @Override
    public int hashCode() {
        int result = seat;
        result = 31 * result + (clazz != null ? clazz.hashCode() : 0);
        result = 31 * result + (passengerResponse != null ? passengerResponse.hashCode() : 0);
        result = 31 * result + (flightResponse != null ? flightResponse.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AddPassengerToFlightResponse{" +
                "seat=" + seat +
                ", clazz='" + clazz + '\'' +
                ", passengerResponse=" + passengerResponse +
                ", flightResponse=" + flightResponse +
                '}';
    }

}

