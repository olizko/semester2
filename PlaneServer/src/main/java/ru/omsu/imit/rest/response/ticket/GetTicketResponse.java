package ru.omsu.imit.rest.response.ticket;

public class GetTicketResponse {
    private int seat;
    private String clazz;

    public GetTicketResponse(int seat, String clazz) {
        this.seat = seat;
        this.clazz = clazz;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GetTicketResponse that = (GetTicketResponse) o;

        if (seat != that.seat) return false;
        return clazz != null ? clazz.equals(that.clazz) : that.clazz == null;
    }

    @Override
    public int hashCode() {
        int result = seat;
        result = 31 * result + (clazz != null ? clazz.hashCode() : 0);
        return result;
    }
}
