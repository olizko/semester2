package ru.omsu.imit.rest.response.passenger;

import ru.omsu.imit.rest.response.ticket.GetTicketResponse;

import java.util.List;
import java.util.Objects;

public class GetPassengerByIdResponse {
    private int passengerId;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String dateOfBirth;
    private List<GetTicketResponse> tickets;

    public GetPassengerByIdResponse(int passengerId, String firstName, String lastName, String patronymic,
                                    String dateOfBirth, List<GetTicketResponse> tickets) {
        this.passengerId = passengerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.dateOfBirth = dateOfBirth;
        this.tickets = tickets;
    }

    public int getPassengerId() {
        return passengerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public List<GetTicketResponse> getTickets() {
        return tickets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GetPassengerByIdResponse that = (GetPassengerByIdResponse) o;
        return passengerId == that.passengerId &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(patronymic, that.patronymic) &&
                Objects.equals(dateOfBirth, that.dateOfBirth) &&
                Objects.equals(tickets, that.tickets);
    }

    @Override
    public int hashCode() {

        return Objects.hash(passengerId, firstName, lastName, patronymic, dateOfBirth, tickets);
    }

    @Override
    public String toString() {
        return "GetPassengerByIdResponse{" +
                "passengerId=" + passengerId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                '}';
    }
}
