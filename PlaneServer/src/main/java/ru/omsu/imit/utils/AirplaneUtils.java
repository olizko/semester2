package ru.omsu.imit.utils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.apache.commons.lang.StringUtils;
import ru.omsu.imit.exception.AirplaneException;
import ru.omsu.imit.rest.response.FailureResponse;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class AirplaneUtils {
    private static final Gson GSON = new Gson();

    public static <T> T getClassInstanceFromJson(Gson gson, String json, Class<T> clazz) throws AirplaneException {
        if (StringUtils.isEmpty(json)) {
            throw new AirplaneException(ErrorCode.NULL_REQUEST);
        }
        try {
            return gson.fromJson(json, clazz);
        } catch (JsonSyntaxException ex) {
            throw new AirplaneException(ErrorCode.JSON_PARSE_EXCEPTION, json);
        }
    }

    public static Response failureResponse(Status status, AirplaneException ex) {
        return Response.status(status).entity(GSON.toJson(new FailureResponse(ex.getErrorCode(), ex.getMessage()))).build();
    }

    public static Response failureResponse(AirplaneException ex) {
        return failureResponse(Status.BAD_REQUEST, ex);
    }

}
