package ru.omsu.imit.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import ru.omsu.imit.model.Flight;
import ru.omsu.imit.model.Plane;


public interface FlightMapper {
    @Insert("INSERT INTO flight (flightFrom, flightTo, date, timeArrival, timeDepartment, planeId) VALUES (#{flightFrom},#{flightTo},#{date},#{timeArrival},#{timeDepartment},#{plane.id})")
    @Options(useGeneratedKeys = true)
    public Integer insert(Flight flight);

    @Select(("SELECT * FROM Flight WHERE id = #{id}"))
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "plane", column = "planeId", javaType = Plane.class, one = @One(select = "ru.omsu.imit.mappers.PlaneMapper.getPlaneById"))
    })
    public Flight getFlightById(@Param("id") int id);

    @Update("UPDATE Flight SET flightFrom = #{flight.flightFrom}, flightTo = #{flight.flightTo}, date = #{flight.date}, timeArrival = #{flight.timeArrival}," +
            "timeDepartment = #{flight.timeDepartment}, " +
            " planeId = #{flight.plane.id} WHERE id = #{id} ")
    public void editFlightById(@Param("id") int id, @Param("flight") Flight flight);

    @Delete("DELETE FROM Flight WHERE id = #{id}")
    public int deleteFlightById(@Param("id") int id);

    @Delete("DELETE FROM Flight")
    public void deleteAllFlight();
}