package ru.omsu.imit.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import ru.omsu.imit.model.Flight;
import ru.omsu.imit.model.Passenger;
import ru.omsu.imit.model.Plane;
import ru.omsu.imit.model.Ticket;

import java.util.List;

public interface PassengerMapper {
    @Insert("INSERT INTO passenger (firstName, lastname, patronymic, dateOfBirth) VALUES ( #{firstName},#{lastName},#{patronymic}, #{dateOfBirth} )")
    @Options(useGeneratedKeys = true)
    public Integer insert(Passenger passenger);

    @Select("SELECT * FROM passenger WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "tickets", column = "id", javaType = List.class, many = @Many(select = "ru.omsu.imit.mappers.TicketMapper.getAllTicketByPassengerId")),
    })
    public Passenger getPassengerById(int id);

    @Select("SELECT passenger.id, firstName, lastname, patronymic, dateOfBirth FROM passenger " +
            "join ticket on passenger.id = ticket.passengerId " +
            "join flight on ticket.flightId = flight.id WHERE flight.id = #{flightId}")
    public List<Passenger> getPassengerByFlightId(@Param("flightId") int flightId);

    @Delete("DELETE FROM  passenger")
    public void deleteAll();

    @Delete("DELETE FROM passenger WHERE id = #{passenger.id}")
    public int deletePassenger(@Param("passenger") Passenger passenger);

    @Delete("DELETE FROM passenger WHERE id = #{id}")
    public int deletePassengerById(@Param("id") int id);

    @Update("UPDATE Passenger SET firstName = #{passenger.firstName}, lastname = #{passenger.lastName}, patronymic = #{passenger.patronymic}, " +
            " dateOfBirth = #{passenger.dateOfBirth} WHERE id = #{id} ")
    public void editPassengerById(@Param("id") int id, @Param("passenger") Passenger passenger);
}
