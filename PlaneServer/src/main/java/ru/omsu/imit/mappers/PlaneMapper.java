package ru.omsu.imit.mappers;

import org.apache.ibatis.annotations.*;
import ru.omsu.imit.model.Plane;

import java.util.List;

public interface PlaneMapper {
    @Insert("INSERT INTO plane (name, capacity) VALUES "
            + "( #{name},#{capacity})")
    @Options(useGeneratedKeys = true)
    public Integer insert(Plane plane);

    @Select("SELECT id, name, capacity FROM Plane WHERE id = #{id}")
    public Plane getPlaneById(int id);

    @Select("SELECT id, name, capacity FROM Plane")
    public List<Plane> getAllLazy();

    @Update("UPDATE Plane SET name = #{plane.name}, capacity = #{plane.capacity}  WHERE id = #{planeId} ")
    public void editPlane(@Param("planeId") int id, @Param("plane") Plane plane);

    @Delete("DELETE FROM Plane WHERE id = #{plane.id}")
    public int delete(@Param("plane") Plane plane);

    @Delete("DELETE FROM Plane WHERE id = #{id}")
    public int deletePlaneById(@Param("id") int id);

    @Delete("DELETE FROM Plane")
    public void deleteAll();

}
