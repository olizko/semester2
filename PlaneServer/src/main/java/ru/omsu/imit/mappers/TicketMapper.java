package ru.omsu.imit.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import ru.omsu.imit.model.Flight;
import ru.omsu.imit.model.Passenger;
import ru.omsu.imit.model.Plane;
import ru.omsu.imit.model.Ticket;

import java.util.List;

public interface TicketMapper {
    @Insert("INSERT INTO ticket (seat, clazz, passengerId, flightId)  VALUES " +
            "(#{seat},#{clazz}, #{passenger.id}, #{flight.id} )")
    @Options(useGeneratedKeys = true)
    public Integer insert(Ticket ticket);

    @Delete("DELETE FROM Ticket WHERE id = #{id}")
    public void deleteTicketById(@Param("id") int id);

    @Delete("DELETE FROM Ticket")
    public void deleteAll();

    @Select("SELECT id, seat, clazz,flightId, passengerId FROM Ticket WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "flight", column = "flightId", javaType = Flight.class, many = @Many(select = "ru.omsu.imit.mappers.FlightMapper.getFlightById", fetchType = FetchType.LAZY)),
            @Result(property = "passenger", column = "passengerId", javaType = Passenger.class, many = @Many(select = "ru.omsu.imit.mappers.PassengerMapper.getPassengerById", fetchType = FetchType.LAZY))
    })
    public Ticket getById(@Param("id") int id);

    @Select("SELECT id, seat, clazz, passengerId, flightId  FROM Ticket WHERE flightId = #{flightId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "flight", column = "flightId", javaType = Flight.class, one = @One(select = "ru.omsu.imit.mappers.FlightMapper.getFlightById")),
            @Result(property = "passenger", column = "passengerId", javaType = Passenger.class, one = @One(select = "ru.omsu.imit.mappers.PassengerMapper.getPassengerById"))
    })
    public List<Ticket> getAllTicketByFlightId(@Param("flightId") int flightId);

    @Select("SELECT id, seat, clazz, passengerId, flightId  FROM Ticket WHERE passengerId = #{passengerId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "flight", column = "flightId", javaType = Flight.class, one = @One(select = "ru.omsu.imit.mappers.FlightMapper.getFlightById")),
            @Result(property = "passenger", column = "passengerId", javaType = Passenger.class, one = @One(select = "ru.omsu.imit.mappers.PassengerMapper.getPassengerById"))
    })
    public List<Ticket> getAllTicketByPassengerId(@Param("passengerId") int passengerId);

    @Update("UPDATE Ticket SET clazz = #{ticket.clazz},  " +
            "  seat = #{ticket.seat}, passengerId = #{ticket.passenger.id},  flightId = #{ticket.flight.id} WHERE id = #{id} ")
    public void editTicket(@Param("id") int id, @Param("ticket") Ticket ticket);

}
