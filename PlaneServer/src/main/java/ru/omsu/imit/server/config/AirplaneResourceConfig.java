package ru.omsu.imit.server.config;

import org.glassfish.jersey.server.ResourceConfig;

public class AirplaneResourceConfig extends ResourceConfig {
    public AirplaneResourceConfig() {
        packages("ru.omsu.imit.resources",
                "ru.omsu.imit.mappers");
    }
}
