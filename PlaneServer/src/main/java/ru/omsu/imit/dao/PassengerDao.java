package ru.omsu.imit.dao;

import ru.omsu.imit.exception.AirplaneException;
import ru.omsu.imit.model.Passenger;

import java.util.List;

public interface PassengerDao  {

    public Passenger insert(Passenger passenger);

    public void deleteAll();

    public void deletePassengerById(int id) throws AirplaneException;

    public Passenger getById(int id);

    public List<Passenger> getPassengersByFlightId (int flightId);

    public void deletePassenger(Passenger passenger);

    public void editPassengerById(int id, Passenger passenger);
}
