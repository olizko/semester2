package ru.omsu.imit.dao;

import ru.omsu.imit.exception.AirplaneException;
import ru.omsu.imit.model.Plane;

import java.sql.Date;
import java.util.List;

public interface PlaneDao {

    public  Plane getById(int id) throws AirplaneException;

    public void editPlane(int id, Plane plane) throws AirplaneException;

    public void deleteById(int id) throws AirplaneException;

    public Plane insert(Plane Plane);

    public List<Plane> getAllLazy();

    public void deleteAll();

    public void delete(Plane Plane);
}
