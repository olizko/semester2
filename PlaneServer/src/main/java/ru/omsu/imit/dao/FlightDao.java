package ru.omsu.imit.dao;

import ru.omsu.imit.model.Flight;

public interface FlightDao {
        public Flight getFlightById(int id);

        public Flight insertFlight(Flight flight);

        public void editFlight(int id, Flight flight);

        public void deleteFlightById(int id);

        public void deleteAllFlight();
}
