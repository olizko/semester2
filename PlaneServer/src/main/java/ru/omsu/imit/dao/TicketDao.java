package ru.omsu.imit.dao;

import ru.omsu.imit.exception.AirplaneException;
import ru.omsu.imit.model.Ticket;

import java.util.List;

public interface TicketDao {
    public Ticket insert(Ticket ticket);

    public void deleteAll();

    public Ticket getById(int id);

    public void deleteTicketById(int id) throws AirplaneException;

    public void editTicket(int id, Ticket ticket);

    public List<Ticket> getAllTicketByFlightId(int flightId);
}
