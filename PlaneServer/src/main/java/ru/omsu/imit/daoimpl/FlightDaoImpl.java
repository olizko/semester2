package ru.omsu.imit.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.dao.FlightDao;
import ru.omsu.imit.model.Flight;

public class FlightDaoImpl extends BaseDaoImpl implements FlightDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(FlightDaoImpl.class);

    @Override
    public Flight insertFlight(Flight flight) {
        LOGGER.debug("DAO Insert Flight {}", flight);
        try (SqlSession sqlSession = getSession()) {
            try {
                getFlightMapper(sqlSession).insert(flight);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't insert flight {} {}", flight, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return flight;
    }

    @Override
    public void editFlight(int id, Flight flight) {
        try (SqlSession sqlSession = getSession()) {
            try {
                getFlightMapper(sqlSession).editFlightById(id, flight);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't change Flight {} {} ", flight, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteFlightById(int id) {
        try (SqlSession sqlSession = getSession()) {
            try {
                getFlightMapper(sqlSession).deleteFlightById(id);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete Flight {} {}", id, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteAllFlight() {
        LOGGER.debug("Dao Delete All Flight {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                getFlightMapper(sqlSession).deleteAllFlight();
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete all Flight {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }

    }


    @Override
    public Flight getFlightById(int id) {
        try (SqlSession sqlSession = getSession()) {
            return getFlightMapper(sqlSession).getFlightById(id);
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get Flight {}", ex);
            throw ex;
        }
    }

}
