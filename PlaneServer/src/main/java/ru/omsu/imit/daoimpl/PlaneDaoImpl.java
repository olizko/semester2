package ru.omsu.imit.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.dao.PlaneDao;
import ru.omsu.imit.exception.AirplaneException;
import ru.omsu.imit.model.Plane;

import java.util.List;

public class PlaneDaoImpl extends BaseDaoImpl implements PlaneDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(PlaneDaoImpl.class);

    @Override
    public Plane insert(Plane Plane) {
        LOGGER.debug("Dao Insert Plane {}", Plane);
        try (SqlSession sqlSession = getSession()) {
            try {
                getPlaneMapper(sqlSession).insert(Plane);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't insert Plane {}, {}", Plane, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return Plane;
    }

    @Override
    public Plane getById(int id) {
        try (SqlSession sqlSession = getSession()) {
            return getPlaneMapper(sqlSession).getPlaneById(id);
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get Plane {}", ex);
            throw ex;
        }
    }

    @Override
    public void editPlane(int id, Plane plane) throws AirplaneException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getPlaneMapper(sqlSession).editPlane(id, plane);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't change Plane Plane Name {} {} ", plane, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }


    @Override
    public void deleteById(int id) {
        try (SqlSession sqlSession = getSession()) {
            try {
                getPlaneMapper(sqlSession).deletePlaneById(id);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete Plane {} {}", id, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }


    @Override
    public List<Plane> getAllLazy() {
        try (SqlSession sqlSession = getSession()) {
            return getPlaneMapper(sqlSession).getAllLazy();
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get All Lazy {}", ex);
            throw ex;
        }
    }


    @Override
    public void delete(Plane Plane) {
        try (SqlSession sqlSession = getSession()) {
            try {
                getPlaneMapper(sqlSession).delete(Plane);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete Plane {} {}", Plane, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("Dao Delete All Plane {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                getPlaneMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete all Plane {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }
}
