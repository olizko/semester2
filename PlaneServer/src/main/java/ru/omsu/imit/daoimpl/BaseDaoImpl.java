package ru.omsu.imit.daoimpl;

import org.apache.ibatis.session.SqlSession;
import ru.omsu.imit.mappers.*;
import ru.omsu.imit.utils.MyBatisUtils;

public class BaseDaoImpl {

    protected SqlSession getSession() {
        return MyBatisUtils.getSqlSessionFactory().openSession();
    }

    protected PlaneMapper getPlaneMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(PlaneMapper.class);
    }

    protected PassengerMapper getPassengerMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(PassengerMapper.class);
    }

    protected TicketMapper getTicketMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(TicketMapper.class);
    }

    protected FlightMapper getFlightMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(FlightMapper.class);
    }
}
