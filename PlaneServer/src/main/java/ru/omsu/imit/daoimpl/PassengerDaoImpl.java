package ru.omsu.imit.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.dao.PassengerDao;
import ru.omsu.imit.exception.AirplaneException;
import ru.omsu.imit.model.Passenger;
import ru.omsu.imit.model.Plane;

import java.util.List;

public class PassengerDaoImpl extends BaseDaoImpl implements PassengerDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlaneDaoImpl.class);

    @Override
    public Passenger insert(Passenger passenger) {
        LOGGER.debug("Dao Insert Passenger {}", passenger);
        try (SqlSession sqlSession = getSession()) {
            try {
                getPassengerMapper(sqlSession).insert(passenger);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't insert Passenger {} {}", passenger, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return passenger;
    }

    @Override
    public Passenger getById(int id) {
        try (SqlSession sqlSession = getSession()) {
            return getPassengerMapper(sqlSession).getPassengerById(id);
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get Passenger by Id {} {}", id, ex);
            throw ex;
        }
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("DAO Delete All Passenger {}");
        try (SqlSession sqlSession = getSession()) {
            try {
                getPassengerMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete all Passenger {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public List<Passenger> getPassengersByFlightId (int flightId){
        try (SqlSession sqlSession = getSession()) {
            return getPassengerMapper(sqlSession).getPassengerByFlightId(flightId);
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get Passengers by FlightId {} {}", flightId, ex);
            throw ex;
        }
    }

    @Override
    public void deletePassenger(Passenger passenger) {
        try (SqlSession sqlSession = getSession()) {
            try {
                getPassengerMapper(sqlSession).deletePassenger(passenger);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete Passenger {} {}", passenger, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void editPassengerById(int id, Passenger passenger) {
        try (SqlSession sqlSession = getSession()) {
            try {
                getPassengerMapper(sqlSession).editPassengerById(id, passenger);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't change Passenger Passenger Name {} {} ", passenger, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deletePassengerById(int id) throws AirplaneException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getPassengerMapper(sqlSession).deletePassengerById(id);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete Passenger by id {} {}", id, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }
}
