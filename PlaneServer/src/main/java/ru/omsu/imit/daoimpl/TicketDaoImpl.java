package ru.omsu.imit.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.dao.TicketDao;
import ru.omsu.imit.exception.AirplaneException;
import ru.omsu.imit.model.Ticket;

import java.util.List;


public class TicketDaoImpl extends BaseDaoImpl implements TicketDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(ru.omsu.imit.daoimpl.TicketDaoImpl.class);

    @Override
    public Ticket insert(Ticket ticket) {
        LOGGER.debug("Dao Insert Flight {} passenger {}", ticket.getFlight(), ticket.getPassenger());
        try (SqlSession sqlSession = getSession()) {
            try {
                getTicketMapper(sqlSession).insert(ticket);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't insert flight {} passenger {} {}", ticket.getFlight(), ticket.getPassenger(), ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
            return ticket;
        }
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("Dao Delete All Flight passengers");
        try (SqlSession sqlSession = getSession()) {
            try {
                getTicketMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete all Flight passengers  {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public Ticket getById(int id) {
        try (SqlSession sqlSession = getSession()) {
            try {
                return getTicketMapper(sqlSession).getById(id);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete all Flight passengers {}", ex);
                throw ex;
            }

        }
    }

    @Override
    public List<Ticket> getAllTicketByFlightId(int flightId) {
        try (SqlSession sqlSession = getSession()) {
            try {
                return getTicketMapper(sqlSession).getAllTicketByFlightId(flightId);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete all Flight passengers {}", ex);
                throw ex;
            }

        }
    }

    @Override
    public void deleteTicketById(int id) throws AirplaneException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getTicketMapper(sqlSession).deleteTicketById(id);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete Ticket {} {}", id, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }

    }

    @Override
    public void editTicket(int id, Ticket ticket) {
        try (SqlSession sqlSession = getSession()) {
            try {
                getTicketMapper(sqlSession).editTicket(id, ticket);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't change Ticket {} {} ", ticket, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }
}