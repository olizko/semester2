package ru.omsu.imit.service.passenger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.dao.PassengerDao;
import ru.omsu.imit.daoimpl.PassengerDaoImpl;
import ru.omsu.imit.exception.AirplaneException;
import ru.omsu.imit.model.Passenger;
import ru.omsu.imit.model.Plane;
import ru.omsu.imit.model.Ticket;
import ru.omsu.imit.rest.request.passenger.EditPassengerRequest;
import ru.omsu.imit.rest.request.passenger.PassengerRequest;
import ru.omsu.imit.rest.response.EmptySuccessResponse;
import ru.omsu.imit.rest.response.passenger.EditPassengerResponse;
import ru.omsu.imit.rest.response.passenger.GetPassengerByIdResponse;
import ru.omsu.imit.rest.response.passenger.GetPassengerResponce;
import ru.omsu.imit.rest.response.passenger.PassengerResponse;
import ru.omsu.imit.rest.response.ticket.GetTicketResponse;
import ru.omsu.imit.service.plane.AirplaneService;
import ru.omsu.imit.utils.AirplaneUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
public class PassengerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AirplaneService.class);
    private static final Gson GSON = new GsonBuilder().create();
    private PassengerDao passengerDao = new PassengerDaoImpl();

    public List<GetPassengerResponce> getPassengerByFlightId(int flightId) {
        List<Passenger> passengerList = passengerDao.getPassengersByFlightId(flightId);

        List<GetPassengerResponce> getPassengerResponces = new ArrayList<>();
        for (Passenger passenger : passengerList) {
            getPassengerResponces.add(new GetPassengerResponce(passenger.getFirstName(), passenger.getLastName(), passenger.getPatronymic()
                    , passenger.getDateOfBirth().toString()));
        }
        return getPassengerResponces;
    }

    public Response editPassengerById(int id, String json) {
        LOGGER.debug("edit passenger By id " + id);
        try {
            EditPassengerRequest request = AirplaneUtils.getClassInstanceFromJson(GSON, json, EditPassengerRequest.class);
            Passenger passenger = new Passenger(request.getFirstName(), request.getLastName(),
                    request.getPatronymic(), LocalDate.parse(request.getDateOfBirth()));
            Passenger editPassenger = passengerDao.getById(id);

            String response = GSON.toJson(new EditPassengerResponse(editPassenger.getFirstName(),
                    editPassenger.getLastName(), editPassenger.getPatronymic(), editPassenger.getDateOfBirth().toString()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (AirplaneException ex) {
            return AirplaneUtils.failureResponse(ex);
        }
    }

    public Response deletePassengerById(int id, String json) {
        LOGGER.debug("delete passenger By id " + id);
        try {
            passengerDao.deletePassengerById(id);
            String response = GSON.toJson(new EmptySuccessResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (AirplaneException ex) {
            return AirplaneUtils.failureResponse(ex);
        }
    }

    public Response getPassengerById(int id) {
        LOGGER.debug("get passenger By id " + id);
        Passenger passenger = passengerDao.getById(id);
        List<GetTicketResponse> ticketsResponse = new ArrayList<>();
        for(Ticket ticket : passenger.getTickets()) {
            ticketsResponse.add(new GetTicketResponse(ticket.getSeat(), ticket.getClazz()));
        }
        String response = GSON.toJson(new GetPassengerByIdResponse(passenger.getId(), passenger.getFirstName(), passenger.getLastName(), passenger.getPatronymic(), passenger.getDateOfBirth().toString(), ticketsResponse));
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    public Response insertPassenger(String json) {
        LOGGER.debug("Insert passenger " + json);
        try {
            PassengerRequest request = AirplaneUtils.getClassInstanceFromJson(GSON, json, PassengerRequest.class);
            Passenger passenger = new Passenger(request.getFirstName(), request.getLastName(), request.getPatronymic(), LocalDate.parse(request.getDateOfBirth()));
            Passenger passenger1 = passengerDao.insert(passenger);
            String response = GSON.toJson(new PassengerResponse(passenger1.getId(), passenger1.getFirstName(),passenger1.getLastName(), passenger1.getPatronymic(),
                    passenger1.getDateOfBirth().toString()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (AirplaneException ex) {
            return AirplaneUtils.failureResponse(ex);
        }
    }

    public void clearDataBase() {
        passengerDao.deleteAll();
    }

}
