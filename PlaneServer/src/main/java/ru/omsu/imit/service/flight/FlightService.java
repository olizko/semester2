package ru.omsu.imit.service.flight;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.dao.PlaneDao;
import ru.omsu.imit.dao.FlightDao;
import ru.omsu.imit.daoimpl.FlightDaoImpl;
import ru.omsu.imit.daoimpl.PlaneDaoImpl;
import ru.omsu.imit.exception.AirplaneException;
import ru.omsu.imit.model.Flight;
import ru.omsu.imit.model.Plane;
import ru.omsu.imit.rest.request.flight.AddFlightRequest;
import ru.omsu.imit.rest.response.flight.AddFlightResponse;
import ru.omsu.imit.rest.response.flight.GetFlightResponse;
import ru.omsu.imit.rest.response.plane.PlaneResponse;
import ru.omsu.imit.utils.AirplaneUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.time.LocalTime;

public class FlightService {
    private static final Logger LOGGER = LoggerFactory.getLogger(FlightService.class);
    private static final Gson GSON = new GsonBuilder().create();
    private FlightDao flightDao = new FlightDaoImpl();
    private PlaneDao planeDao = new PlaneDaoImpl();

    public Response getToFlight(int id) {
        LOGGER.debug("get By id " + id);
        Flight flight = flightDao.getFlightById(id);
        String response = GSON.toJson(new GetFlightResponse(flight.getId(), flight.getFlightFrom(), flight.getFlightTo(),
                flight.getDate().toString(), flight.getTimeArrival().toString(), flight.getTimeDepartment().toString(),
                new PlaneResponse(flight.getPlane().getId(), flight.getPlane().getName(), flight.getPlane().getCapacity())));
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    public Response addFlight(String json, int id) throws AirplaneException {
        LOGGER.debug("add Flight " + json + " Plane id " + id);
        try {
            AddFlightRequest flightRequest = AirplaneUtils.getClassInstanceFromJson(GSON, json, AddFlightRequest.class);
            Plane plane = planeDao.getById(id);
            Flight flight = new Flight(flightRequest.getFlightFrom(), flightRequest.getFlightTo(), LocalDate.parse(flightRequest.getDate()),
                    LocalTime.parse(flightRequest.getTimeArrival()), LocalTime.parse(flightRequest.getTimeDepartment()), plane);
            flightDao.insertFlight(flight);
            String response = GSON.toJson(new AddFlightResponse(flight.getId(),flight.getFlightFrom(), flight.getFlightTo(),
                    flight.getDate().toString(), flight.getTimeArrival().toString(), flight.getTimeDepartment().toString(),
                    flight.getPlane().getId()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (AirplaneException ex) {
            return AirplaneUtils.failureResponse(ex);
        }
    }
}
