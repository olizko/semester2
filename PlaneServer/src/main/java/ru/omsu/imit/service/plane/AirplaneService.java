package ru.omsu.imit.service.plane;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.dao.PassengerDao;
import ru.omsu.imit.dao.PlaneDao;
import ru.omsu.imit.dao.TicketDao;
import ru.omsu.imit.daoimpl.PassengerDaoImpl;
import ru.omsu.imit.daoimpl.PlaneDaoImpl;
import ru.omsu.imit.daoimpl.TicketDaoImpl;
import ru.omsu.imit.exception.AirplaneException;
import ru.omsu.imit.model.Plane;
import ru.omsu.imit.rest.request.plane.EditPlaneRequest;
import ru.omsu.imit.rest.request.plane.PlaneRequest;
import ru.omsu.imit.rest.response.EmptySuccessResponse;
import ru.omsu.imit.rest.response.plane.EditPlaneResponse;
import ru.omsu.imit.rest.response.plane.GetPlaneResponse;
import ru.omsu.imit.rest.response.plane.PlaneResponse;
import ru.omsu.imit.utils.AirplaneUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class AirplaneService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AirplaneService.class);
    private static final Gson GSON = new GsonBuilder().create();
    private PlaneDao planeDao = new PlaneDaoImpl();
    private PassengerDao passengerDao = new PassengerDaoImpl();
    private TicketDao ticketDao = new TicketDaoImpl();

    public Response getById(int id) {
        LOGGER.debug("get By id " + id);
        try {
            Plane plane = planeDao.getById(id);
            String response = GSON.toJson(new GetPlaneResponse(plane.getId(), plane.getName(), plane.getCapacity()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (AirplaneException ex) {
            return AirplaneUtils.failureResponse(ex);
        }
    }

    public Response editById(int id, String json) {
        LOGGER.debug("edit By id " + id);
        try {
            EditPlaneRequest request = AirplaneUtils.getClassInstanceFromJson(GSON, json, EditPlaneRequest.class);
            Plane plane = new Plane(request.getName(), request.getCapacity());
            planeDao.editPlane(id, plane);
            Plane editPlane = planeDao.getById(id);
            String response = GSON.toJson(new EditPlaneResponse(editPlane.getName(), editPlane.getCapacity()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (AirplaneException ex) {
            return AirplaneUtils.failureResponse(ex);
        }
    }

    public Response deleteById(int id) {
        LOGGER.debug("delete By id " + id);
        try {
            planeDao.deleteById(id);
            String response = GSON.toJson(new EmptySuccessResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (AirplaneException ex) {
            return AirplaneUtils.failureResponse(ex);
        }
    }

    public Response insertPlane(String json) {
        LOGGER.debug("Insert  plane " + json);
        try {
            PlaneRequest request = AirplaneUtils.getClassInstanceFromJson(GSON, json, PlaneRequest.class);
            Plane plane = new Plane(request.getName(), request.getCapacity());
            Plane addPlane = planeDao.insert(plane);
            String response = GSON.toJson(new PlaneResponse(addPlane.getId(), addPlane.getName(), addPlane.getCapacity()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (AirplaneException ex) {
            return AirplaneUtils.failureResponse(ex);
        }
    }

    public Response clearDataBase() {
        ticketDao.deleteAll();
        planeDao.deleteAll();
        passengerDao.deleteAll();
        return Response.ok(new EmptySuccessResponse(), MediaType.APPLICATION_JSON).build();
    }
}