package ru.omsu.imit.service.ticket;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.dao.FlightDao;
import ru.omsu.imit.dao.PassengerDao;
import ru.omsu.imit.dao.PlaneDao;
import ru.omsu.imit.dao.TicketDao;
import ru.omsu.imit.daoimpl.FlightDaoImpl;
import ru.omsu.imit.daoimpl.PassengerDaoImpl;
import ru.omsu.imit.daoimpl.PlaneDaoImpl;
import ru.omsu.imit.daoimpl.TicketDaoImpl;
import ru.omsu.imit.exception.AirplaneException;
import ru.omsu.imit.model.Flight;
import ru.omsu.imit.model.Passenger;
import ru.omsu.imit.model.Ticket;
import ru.omsu.imit.rest.request.passenger.AddPassengerToTicketRequest;
import ru.omsu.imit.rest.response.flight.GetFlightResponse;
import ru.omsu.imit.rest.response.passenger.AddPassengerToFlightResponse;
import ru.omsu.imit.rest.response.passenger.PassengerResponse;
import ru.omsu.imit.rest.response.plane.PlaneResponse;
import ru.omsu.imit.service.plane.AirplaneService;
import ru.omsu.imit.utils.AirplaneUtils;
import ru.omsu.imit.utils.ErrorCode;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class TicketService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AirplaneService.class);
    private static final Gson GSON = new GsonBuilder().create();
    private TicketDao ticketDao = new TicketDaoImpl();
    private PassengerDao passengerDao = new PassengerDaoImpl();
    private PlaneDao planeDao = new PlaneDaoImpl();
    private FlightDao flightDao = new FlightDaoImpl();




    public Response addPassengerToTicket(String json) throws AirplaneException {
        AddPassengerToTicketRequest addPassengerToTicketRequest = GSON.fromJson(json, AddPassengerToTicketRequest.class);
        Passenger passenger = passengerDao.getById(addPassengerToTicketRequest.getPassengerId());
        if (passenger == null) {
            return AirplaneUtils.failureResponse(new AirplaneException(ErrorCode.PASSENGER_NOT_FOUND));
        }

        List<Passenger> passengers = new ArrayList<>();
        passengers.add(passenger);

        Flight flight = flightDao.getFlightById(addPassengerToTicketRequest.getFlightId());
        if (flight == null) {
            return AirplaneUtils.failureResponse(new AirplaneException(ErrorCode.PLANE_NOT_FOUND));
        }

        Ticket ticket = new Ticket(addPassengerToTicketRequest.getSeat(), addPassengerToTicketRequest.getClazz(),
                 passenger, flight);
        Ticket responseTicket = ticketDao.insert(ticket);
        PassengerResponse passengerResponse = new PassengerResponse(passenger.getId(), passenger.getFirstName(), passenger.getLastName(), passenger.getPatronymic(), passenger.getDateOfBirth().toString());
        GetFlightResponse flightResponse = new GetFlightResponse(flight.getId(), flight.getFlightFrom(),
                flight.getFlightTo(),flight.getDate().toString(),flight.getTimeArrival().toString(),
                flight.getTimeDepartment().toString(), new PlaneResponse(flight.getPlane().getId(), flight.getPlane().getName(),
                flight.getPlane().getCapacity()));
        AddPassengerToFlightResponse addPassengerToFlightResponse = new AddPassengerToFlightResponse(ticket.getSeat(),
                ticket.getClazz(), passengerResponse, flightResponse);
        String response = GSON.toJson(addPassengerToFlightResponse);
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }
}
