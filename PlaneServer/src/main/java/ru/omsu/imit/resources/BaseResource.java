package ru.omsu.imit.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import ru.omsu.imit.service.plane.AirplaneService;

import javax.ws.rs.DELETE;
import javax.ws.rs.Path;

@Path("/api")
public class BaseResource {

    private static AirplaneService airplaneService = new AirplaneService();

    @DELETE
    @Path("/clear")
    @Consumes("application/json")
    @Produces("application/json")
    public Response clearDataBase() {
        return airplaneService.clearDataBase();
    }

}
