package ru.omsu.imit.resources;

import ru.omsu.imit.exception.AirplaneException;
import ru.omsu.imit.service.flight.FlightService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
@Path("/api")
public class FlightResource {
    private static FlightService flightService = new FlightService();

    @GET
    @Path("/flight/{id}")
    @Produces("application/json")
    public Response getToFlight(@PathParam(value = "id") int id) {
        return flightService.getToFlight(id);
    }

    @POST
    @Path("/flight/{id}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addFlight(String json, @PathParam(value = "id") int id) throws AirplaneException {
        return flightService.addFlight(json,id);
    }
}
