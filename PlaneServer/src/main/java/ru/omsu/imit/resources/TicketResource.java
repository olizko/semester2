package ru.omsu.imit.resources;

import ru.omsu.imit.exception.AirplaneException;
import ru.omsu.imit.service.ticket.TicketService;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/api")
public class TicketResource {
    private static TicketService ticketService = new TicketService();

    @POST
    @Path("/appointment")
    @Produces("application/json")
    public Response addPassengerToPlane(String json) throws AirplaneException {
        return ticketService.addPassengerToTicket(json);
    }
}
