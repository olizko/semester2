package ru.omsu.imit.resources;

import ru.omsu.imit.rest.response.passenger.GetPassengerResponce;
import ru.omsu.imit.service.passenger.PassengerService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/api")
public class PassengerResource {
    private static PassengerService passengerService = new PassengerService();

    @GET
    @Path("/passenger/{flight_id}")
    @Produces("application/json")
    public List<GetPassengerResponce> getFlightToPassengerById(@PathParam(value = "flight_id") int id) {
        return passengerService.getPassengerByFlightId(id);
    }

    @POST
    @Path("/airplane/passenger")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addPassenger(String json) {
        return passengerService.insertPassenger(json);
    }

    @GET
    @Path("/passengers/{id}")
    @Produces("application/json")
    public Response getPassengerById(@PathParam(value = "id") int id) {
        return passengerService.getPassengerById(id);
    }

    @PUT
    @Path("/passenger/{id}")
    @Produces("application/json")
    public Response editPassengerById(@PathParam(value = "id") int id, String json) {
        return passengerService.editPassengerById(id, json);
    }

    @DELETE
    @Path("/passenger/{id}")
    @Produces("application/json")
    public Response deletePassengerById(@PathParam(value = "id") int id, String json) {
        return passengerService.deletePassengerById(id, json);
    }
}
