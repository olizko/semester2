package ru.omsu.imit.resources;

import com.google.gson.Gson;
import ru.omsu.imit.rest.request.plane.PlaneRequest;
import ru.omsu.imit.service.plane.AirplaneService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/api")
public class PlaneResource {
    private static AirplaneService airplaneService = new AirplaneService();

    @POST
    @Path("/airplane/plane")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addPlane(String json) {
        return airplaneService.insertPlane(json);
    }

    @GET
    @Path("/plane/{id}")
    @Produces("application/json")
    public Response getPlaneById(@PathParam(value = "id") int id) {
        return airplaneService.getById(id);
    }

    @PUT
    @Path("/airplane/{id}")
    @Produces("application/json")
    public Response editById(@PathParam(value = "id") int id, String json) {
        return airplaneService.editById(id, json);
    }

    @DELETE
    @Path("/airplane/{id}")
    @Produces("application/json")
    public Response deleteById(@PathParam(value = "id") int id) {
        return airplaneService.deleteById(id);
    }
}
