package ru.omsu.imit.model;

import java.time.LocalDate;
import java.util.Objects;

public class Plane {
    private int id;
    private String name;
    private String capacity;

    public Plane(int id, String name, String capacity) {
        this.id = id;
        this.name = name;
        this.capacity = capacity;
    }

    public Plane(String name, String capacity){
        this(0, name, capacity);
    }

    public Plane() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Plane plane = (Plane) o;

        if (id != plane.id) return false;
        if (name != null ? !name.equals(plane.name) : plane.name != null) return false;
        return capacity != null ? capacity.equals(plane.capacity) : plane.capacity == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (capacity != null ? capacity.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Plane{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", capacity='" + capacity + '\'' +
                '}';
    }
}
