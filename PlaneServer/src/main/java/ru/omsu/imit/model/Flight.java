package ru.omsu.imit.model;

import java.time.LocalDate;
import java.time.LocalTime;

public class Flight {
    private int id;
    private String flightFrom;
    private String flightTo;
    private LocalDate date;
    private LocalTime timeArrival;
    private LocalTime timeDepartment;
    private Plane plane;

    private Flight() {}

    public Flight(int id, String flightFrom, String flightTo, LocalDate date, LocalTime timeArrival, LocalTime timeDepartment, Plane plane) {
        this.id = id;
        this.flightFrom = flightFrom;
        this.flightTo = flightTo;
        this.date = date;
        this.timeArrival = timeArrival;
        this.timeDepartment = timeDepartment;
        this.plane = plane;
    }


    public Flight(String flightFrom, String flightTo, LocalDate date, LocalTime timeArrival, LocalTime timeDepartment, Plane plane) {
        this(0, flightFrom,flightTo,date,timeArrival,timeDepartment, plane);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFlightFrom() {
        return flightFrom;
    }

    public void setFlightFrom(String flightFrom) {
        this.flightFrom = flightFrom;
    }

    public String getFlightTo() {
        return flightTo;
    }

    public void setFlightTo(String flightTo) {
        this.flightTo = flightTo;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTimeDepartment() {
        return timeDepartment;
    }

    public void setTimeDepartment(LocalTime timeDepartment) {
        this.timeDepartment = timeDepartment;
    }

    public LocalTime getTimeArrival() {
        return timeArrival;
    }

    public void setTimeArrival(LocalTime timeArrival) {
        this.timeArrival = timeArrival;
    }

    public Plane getPlane() {
        return plane;
    }

    public void setPlane(Plane plane) {
        this.plane = plane;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Flight flight = (Flight) o;

        if (id != flight.id) return false;
        if (flightFrom != null ? !flightFrom.equals(flight.flightFrom) : flight.flightFrom != null) return false;
        if (flightTo != null ? !flightTo.equals(flight.flightTo) : flight.flightTo != null) return false;
        if (date != null ? !date.equals(flight.date) : flight.date != null) return false;
        if (timeArrival != null ? !timeArrival.equals(flight.timeArrival) : flight.timeArrival != null) return false;
        if (timeDepartment != null ? !timeDepartment.equals(flight.timeDepartment) : flight.timeDepartment != null)
            return false;
        return plane != null ? plane.equals(flight.plane) : flight.plane == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (flightFrom != null ? flightFrom.hashCode() : 0);
        result = 31 * result + (flightTo != null ? flightTo.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (timeArrival != null ? timeArrival.hashCode() : 0);
        result = 31 * result + (timeDepartment != null ? timeDepartment.hashCode() : 0);
        result = 31 * result + (plane != null ? plane.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", flightFrom='" + flightFrom + '\'' +
                ", flightTo='" + flightTo + '\'' +
                ", date=" + date +
                ", timeArrival=" + timeArrival +
                ", timeDepartment=" + timeDepartment +
                ", plane=" + plane +
                '}';
    }
}