package ru.omsu.imit.model;

import java.util.List;

public class Ticket {
    private int id;
    private int seat;
    private String clazz;
    private Passenger passenger;
    private Flight flight;


    private Ticket(){}


    public Ticket(int id, int seat, String clazz, Passenger passenger, Flight flight) {
        this.id = id;
        this.seat = seat;
        this.clazz = clazz;
        this.passenger = passenger;
        this.flight = flight;
    }

    public Ticket(int seat, String clazz, Passenger passenger, Flight flight) {
        this(0,seat, clazz, passenger, flight);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ticket ticket = (Ticket) o;

        if (id != ticket.id) return false;
        if (seat != ticket.seat) return false;
        if (clazz != null ? !clazz.equals(ticket.clazz) : ticket.clazz != null) return false;
        if (passenger != null ? !passenger.equals(ticket.passenger) : ticket.passenger != null) return false;
        return flight != null ? flight.equals(ticket.flight) : ticket.flight == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + seat;
        result = 31 * result + (clazz != null ? clazz.hashCode() : 0);
        result = 31 * result + (passenger != null ? passenger.hashCode() : 0);
        result = 31 * result + (flight != null ? flight.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", seat=" + seat +
                ", clazz='" + clazz + '\'' +
                '}';
    }
}
