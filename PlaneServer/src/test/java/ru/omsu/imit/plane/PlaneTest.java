package ru.omsu.imit.plane;

import org.junit.Test;
import ru.omsu.imit.base.BaseClientTest;
import ru.omsu.imit.exception.AirplaneException;
import ru.omsu.imit.model.Plane;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PlaneTest extends BaseClientTest {
    @Test
    public void testInsertPlane() throws ArithmeticException, AirplaneException {
        Plane plane = new Plane("Cy-100","100");
        planeDao.insert(plane);
        Plane planeFromDB = planeDao.getById(plane.getId());
        checkPlaneFields(plane, planeFromDB);
    }


    @Test
    public void testDeletePlane() throws AirplaneException {
        Plane plane = new Plane("Cy-100","100");
        planeDao.insert(plane);
        Plane planeFromDB = planeDao.getById(plane.getId());
        checkPlaneFields(plane, planeFromDB);
        planeDao.delete(plane);
        planeFromDB = planeDao.getById(plane.getId());
        assertNull(planeFromDB);
    }

    @Test
    public void testDeletePlaneById() throws AirplaneException {
        Plane plane = new Plane("Cy-100","100");
        planeDao.insert(plane);
        planeDao.deleteById(plane.getId());
        Plane plane1 = planeDao.getById(plane.getId());
        assertNull(plane1);
    }

    @Test
    public void testGetPlaneById() throws AirplaneException {
        Plane plane = new Plane("Cy-100","100");
        Plane plane1 = planeDao.insert(plane);
        Plane plane2 = planeDao.getById(plane1.getId());
        assertEquals(plane1.getId(), plane2.getId());
    }

    @Test
    public void testGetAllPlane() throws AirplaneException {
        Plane plane1 = new Plane("Cy-100","100");
        Plane plane2 = new Plane("Cy-27", "2");
        Plane plane3 = new Plane("Airbus-A318", "132");
        Plane plane4 = new Plane("Airbus", "128");
        Plane insertPlane1 = planeDao.insert(plane1);
        Plane insertPlane2 = planeDao.insert(plane2);
        Plane insertPlane3 = planeDao.insert(plane3);
        Plane insertPlane4 = planeDao.insert(plane4);
        List<Plane> planeList = planeDao.getAllLazy();
        assertEquals(planeList.size(), 4);
    }

    @Test
    public void testEditPlane() throws AirplaneException {
        Plane plane1 = new Plane("Cy-100","100");
        Plane plane2 = new Plane("Cy-27", "2");
        Plane plane = planeDao.insert(plane1);
        planeDao.editPlane(plane.getId(), plane2);
        Plane plane3 = planeDao.getById(plane1.getId());
        assertEquals(plane3.getName(), plane2.getName());
    }

    @Test
    public void testDeleteAllPlane() throws AirplaneException {

        Plane plane1 = new Plane("Cy-100","100" );
        Plane plane2 = new Plane("Cy-27", "2");
        Plane plane3 = new Plane("Airbus-A318",  "132");
        Plane plane4 = new Plane("Cy-12","50");
        planeDao.insert(plane1);
        planeDao.insert(plane2);
        planeDao.insert(plane3);
        planeDao.insert(plane4);
        planeDao.deleteAll();
        Plane plane = planeDao.getById(plane1.getId());
        assertNull(plane);
        planeDao.deleteAll();
    }
}
