package ru.omsu.imit.base;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.client.AirplaneClient;
import ru.omsu.imit.rest.response.FailureResponse;
import ru.omsu.imit.rest.response.passenger.AddPassengerToFlightResponse;
import ru.omsu.imit.rest.response.passenger.EditPassengerResponse;
import ru.omsu.imit.rest.response.passenger.GetPassengerByIdResponse;
import ru.omsu.imit.rest.response.passenger.PassengerResponse;
import ru.omsu.imit.rest.response.plane.EditPlaneResponse;
import ru.omsu.imit.rest.response.plane.GetPlaneResponse;
import ru.omsu.imit.rest.response.plane.PlaneResponse;
import ru.omsu.imit.rest.response.flight.AddFlightResponse;
import ru.omsu.imit.rest.response.flight.GetFlightResponse;
import ru.omsu.imit.server.AirplaneServer;
import ru.omsu.imit.server.config.Settings;
import ru.omsu.imit.service.plane.AirplaneService;
import ru.omsu.imit.utils.ErrorCode;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BaseClientTest extends BaseDaoTest {


    private static final Logger LOGGER = LoggerFactory.getLogger(BaseClientTest.class);

    protected static AirplaneClient client = new AirplaneClient();
    protected static String baseURL;

    private static void initialize() {
        String hostName = null;
        try {
            hostName = InetAddress.getLocalHost().getCanonicalHostName();
        } catch (UnknownHostException e) {
            LOGGER.debug("Can't determine my own host name", e);
        }
        baseURL = "http://" + hostName + ":" + Settings.getRestHTTPPort() + "/api";
    }

    @BeforeClass
    public static void startServer() {
        initialize();
        AirplaneServer.createServer();
    }

    @AfterClass
    public static void stopServer() {
        AirplaneServer.stopServer();
    }

    @Before
    public void clearDataBase() {
//        client.delete(baseURL + "/clear", EmptySuccessResponse.class);
        AirplaneService airplaneService = new AirplaneService();
        airplaneService.clearDataBase();
    }

    public static String getBaseURL() {
        return baseURL;
    }

    protected void checkFailureResponse(Object response, ErrorCode expectedStatus) {
        assertTrue(response instanceof FailureResponse);
        FailureResponse failureResponseObject = (FailureResponse) response;
        assertEquals(expectedStatus, failureResponseObject.getErrorCode());
    }

    protected void checkPassengerResponse(PassengerResponse passengerResponse, String firstName, String lastName, String patronymic, String dateOfBirth) {
        assertEquals(firstName, passengerResponse.getFirstName());
        assertEquals(lastName, passengerResponse.getLastName());
        assertEquals(patronymic, passengerResponse.getPatronymic());
        assertEquals(dateOfBirth, passengerResponse.getDateOfBirth());
    }

    protected void checkEditPassengerResponse(EditPassengerResponse passengerResponse, String firstName, String lastName, String patronymic, String dateOfBirth) {
        assertEquals(firstName, passengerResponse.getFirstName());
        assertEquals(lastName, passengerResponse.getLastName());
        assertEquals(patronymic, passengerResponse.getPatronymic());
        assertEquals(dateOfBirth, passengerResponse.getDateOfBirth());
    }

    protected void checkGetPassengerByIdResponse(GetPassengerByIdResponse passengerByIdResponse, String firstName, String lastName, String
            patronymic , String dateOfBirth) {
        assertEquals(firstName, passengerByIdResponse.getFirstName());
        assertEquals(lastName, passengerByIdResponse.getLastName());
        assertEquals(patronymic, passengerByIdResponse.getPatronymic());
        assertEquals(dateOfBirth, passengerByIdResponse.getDateOfBirth());
    }


    protected void checkPlaneResponse(PlaneResponse planeResponse, String name, String capacity) {
        assertEquals(name, planeResponse.getName());
        assertEquals(capacity, planeResponse.getCapacity());
    }

    protected void checkEditPlaneResponse(EditPlaneResponse planeResponse, String name, String capacity) {
        assertEquals(name, planeResponse.getName());
        assertEquals(capacity, planeResponse.getCapacity());
    }

    protected void checkGetPlaneResponse(GetPlaneResponse plane, String name, String capacity) {
        assertEquals(name, plane.getName());
        assertEquals(capacity, plane.getCapacity());
    }

    protected void checkTicketResponse(AddPassengerToFlightResponse passengerToPlaneResponse, int seat, String clazz, int flightId, int passengerId) {
        assertEquals(clazz, passengerToPlaneResponse.getClazz());
        assertEquals(seat, passengerToPlaneResponse.getSeat());
        assertEquals(flightId, passengerToPlaneResponse.getFlightResponse().getFlightId());
        assertEquals(passengerId, passengerToPlaneResponse.getPassengerResponse().getId());
    }

    protected void checkFlightResponse(AddFlightResponse flightResponse,String flightFrom, String flightTo, String date, String timeArrival, String timeDepartment) {
        assertEquals(flightFrom, flightResponse.getFlightFrom());
        assertEquals(flightTo, flightResponse.getFlightTo());
        assertEquals(date, flightResponse.getDate());
        assertEquals(timeArrival, flightResponse.getTimeArrival());
        assertEquals(timeDepartment, flightResponse.getTimeDepartment());
    }

    protected void checkGetFlightResponse(GetFlightResponse flightResponse, String flightFrom, String flightTo, String date, String timeArrival, String timeDepartment) {
        assertEquals(flightFrom, flightResponse.getFlightFrom());
        assertEquals(flightTo, flightResponse.getFlightTo());
        assertEquals(date, flightResponse.getDate());
        assertEquals(timeArrival, flightResponse.getTimeArrival());
        assertEquals(timeDepartment, flightResponse.getTimeDepartment());
    }
}