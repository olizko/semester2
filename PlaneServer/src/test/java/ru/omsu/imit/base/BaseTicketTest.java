package ru.omsu.imit.base;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.rest.request.flight.AddFlightRequest;
import ru.omsu.imit.rest.request.passenger.AddPassengerToTicketRequest;
import ru.omsu.imit.rest.request.plane.PlaneRequest;
import ru.omsu.imit.rest.request.passenger.PassengerRequest;
import ru.omsu.imit.rest.response.flight.AddFlightResponse;
import ru.omsu.imit.rest.response.passenger.AddPassengerToFlightResponse;
import ru.omsu.imit.rest.response.plane.PlaneResponse;
import ru.omsu.imit.rest.response.passenger.GetPassengerResponce;
import ru.omsu.imit.rest.response.passenger.PassengerResponse;
import ru.omsu.imit.utils.ErrorCode;


import java.util.List;

import static org.junit.Assert.assertEquals;


public class BaseTicketTest extends  BaseClientTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseClientTest.class);


    protected AddPassengerToFlightResponse addPassengerToPlaneResponce( int seat, String clazz, int flightId,
                                                                       int passengerId, ErrorCode expectedStatus) {
        AddPassengerToTicketRequest request = new AddPassengerToTicketRequest(seat, clazz, passengerId, flightId);
        Object response = client.post(baseURL + "/appointment", request, AddPassengerToFlightResponse.class);
        if (response instanceof AddPassengerToFlightResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            AddPassengerToFlightResponse addTicketResponse = (AddPassengerToFlightResponse) response;
            checkTicketResponse(addTicketResponse, seat, clazz, flightId, passengerId);
            return addTicketResponse ;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected PassengerResponse addPassenger(String firstName, String lastname, String patronymic, String dateOfBirth, ErrorCode expectedStatus) {
        PassengerRequest request = new PassengerRequest(firstName, lastname, patronymic, dateOfBirth);
        Object response = client.post(baseURL + "/airplane/passenger", request, PassengerResponse.class);
        if (response instanceof PassengerResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            PassengerResponse addPassengerResponse = (PassengerResponse) response;
            checkPassengerResponse(addPassengerResponse, firstName, lastname, patronymic, dateOfBirth);
            return addPassengerResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected PlaneResponse addPlane(String name, String capacity, ErrorCode expectedStatus) {
        PlaneRequest request = new PlaneRequest(name, capacity);
        Object response = client.post(baseURL + "/airplane/plane", request, PlaneResponse.class);
        if (response instanceof PlaneResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            PlaneResponse addPlaneResponse = (PlaneResponse) response;
            checkPlaneResponse(addPlaneResponse, name, capacity);
            return addPlaneResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected List<GetPassengerResponce> getFlightToPassengerById(int id, int expectedCount, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/passenger/" + id, List.class);
        if (response instanceof List<?>) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            @SuppressWarnings("unchecked")
            List<GetPassengerResponce> getPassengerResponse = (List<GetPassengerResponce>) response;
            assertEquals(expectedCount, getPassengerResponse.size());
            return getPassengerResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected AddFlightResponse addFlight(String flightFrom, String flightTo, String date, String timeArrival, String timeDepartment, int planeId, ErrorCode expectedStatus) {
        AddFlightRequest request = new AddFlightRequest(flightFrom,flightTo,date,timeArrival,timeDepartment);
        Object response = client.post(baseURL + "/flight/" + planeId, request, AddFlightResponse.class);
        if (response instanceof AddFlightResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            AddFlightResponse addFlightResponse = (AddFlightResponse) response;
            checkFlightResponse(addFlightResponse, flightFrom,flightTo,date,timeArrival,timeDepartment);
            return addFlightResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }
}
