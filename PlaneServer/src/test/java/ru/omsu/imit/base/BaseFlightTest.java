package ru.omsu.imit.base;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.rest.request.flight.AddFlightRequest;
import ru.omsu.imit.rest.request.plane.PlaneRequest;
import ru.omsu.imit.rest.response.flight.AddFlightResponse;
import ru.omsu.imit.rest.response.flight.GetFlightResponse;
import ru.omsu.imit.rest.response.plane.PlaneResponse;
import ru.omsu.imit.utils.ErrorCode;

import static org.junit.Assert.assertEquals;

public class BaseFlightTest extends BaseClientTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseClientTest.class);

    protected AddFlightResponse addFlight(String flightFrom, String flightTo, String date, String timeArrival, String timeDepartment, int planeId, ErrorCode expectedStatus) {
        AddFlightRequest request = new AddFlightRequest(flightFrom,flightTo,date,timeArrival,timeDepartment);
        Object response = client.post(baseURL + "/flight/" + planeId, request, AddFlightResponse.class);
        if (response instanceof AddFlightResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            AddFlightResponse addFlightResponse = (AddFlightResponse) response;
            checkFlightResponse(addFlightResponse, flightFrom,flightTo,date,timeArrival,timeDepartment);
            return addFlightResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }

    }

    protected GetFlightResponse getFlightById(int flightId, String flightFrom, String flightTo, String date, String timeArrival, String timeDepartment, int planeResponse, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/flight/" + flightId, GetFlightResponse.class);
        if (response instanceof GetFlightResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            GetFlightResponse getflightResponse = (GetFlightResponse) response;
            assertEquals(flightId, getflightResponse.getFlightId());
            checkGetFlightResponse(getflightResponse, flightFrom, flightTo, date, timeArrival , timeDepartment);
            return getflightResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected PlaneResponse addPlane(String name, String capacity, ErrorCode expectedStatus) {
        PlaneRequest request = new PlaneRequest(name, capacity);
        Object response = client.post(baseURL + "/airplane/plane", request, PlaneResponse.class);
        if (response instanceof PlaneResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            PlaneResponse addPlaneResponse = (PlaneResponse) response;
            checkPlaneResponse(addPlaneResponse, name, capacity);
            return addPlaneResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

}
