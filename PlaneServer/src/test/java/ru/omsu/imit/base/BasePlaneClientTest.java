package ru.omsu.imit.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.rest.request.plane.PlaneRequest;
import ru.omsu.imit.rest.request.plane.EditPlaneRequest;
import ru.omsu.imit.rest.response.EmptySuccessResponse;
import ru.omsu.imit.rest.response.plane.PlaneResponse;
import ru.omsu.imit.rest.response.plane.EditPlaneResponse;
import ru.omsu.imit.rest.response.plane.GetPlaneResponse;
import ru.omsu.imit.utils.ErrorCode;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class BasePlaneClientTest extends BaseClientTest {


    protected PlaneResponse addPlane(String name, String capacity,  ErrorCode expectedStatus) {
        PlaneRequest request = new PlaneRequest(name, capacity);
        Object response = client.post(baseURL + "/airplane/plane", request, PlaneResponse.class);
        if (response instanceof PlaneResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            PlaneResponse addPlaneResponse = (PlaneResponse) response;
            checkPlaneResponse(addPlaneResponse, name, capacity);
            return addPlaneResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected EmptySuccessResponse deletePlaneById(int id, ErrorCode expectedStatus) {
        Object response = client.delete(baseURL + "/airplane/" + id, EmptySuccessResponse.class);
        if (response instanceof EmptySuccessResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            return (EmptySuccessResponse) response;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected EditPlaneResponse editPlaneById(int id, String name, String capacity, ErrorCode expectedStatus) {
        EditPlaneRequest request = new EditPlaneRequest(name, capacity);
        Object response = client.put(baseURL + "/airplane/" + id, request, EditPlaneResponse.class);
        if (response instanceof EditPlaneResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            EditPlaneResponse editPlaneResponse = (EditPlaneResponse) response;
            checkEditPlaneResponse(editPlaneResponse, name, capacity);
            return editPlaneResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected GetPlaneResponse getPlaneById(int id, String name, String capacity, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/plane/" + id, GetPlaneResponse.class);
        if (response instanceof GetPlaneResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            GetPlaneResponse getPlaneResponse = (GetPlaneResponse) response;
            assertEquals(id, getPlaneResponse.getPlaneId());
            checkGetPlaneResponse(getPlaneResponse, name, capacity);
            return getPlaneResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }
}
