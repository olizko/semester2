package ru.omsu.imit.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static org.junit.Assert.assertEquals;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.rest.request.passenger.EditPassengerRequest;
import ru.omsu.imit.rest.request.passenger.PassengerRequest;
import ru.omsu.imit.rest.response.EmptySuccessResponse;
import ru.omsu.imit.rest.response.passenger.EditPassengerResponse;
import ru.omsu.imit.rest.response.passenger.GetPassengerByIdResponse;
import ru.omsu.imit.rest.response.passenger.GetPassengerResponce;
import ru.omsu.imit.rest.response.passenger.PassengerResponse;
import ru.omsu.imit.utils.ErrorCode;


public class BasePassengerClientTest extends BaseClientTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseClientTest.class);

    protected PassengerResponse addPassenger(String firstName, String lastname, String patronymic, String dateOfBirth, ErrorCode expectedStatus) {
        PassengerRequest request = new PassengerRequest(firstName, lastname, patronymic, dateOfBirth);
        Object response = client.post(baseURL + "/airplane/passenger", request, PassengerResponse.class);
        if (response instanceof PassengerResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            PassengerResponse addPassengerResponse = (PassengerResponse) response;
            checkPassengerResponse(addPassengerResponse, firstName, lastname, patronymic, dateOfBirth);
            return addPassengerResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected EmptySuccessResponse deletePassengerById(int id, ErrorCode expectedStatus) {
        Object response = client.delete(baseURL + "/passenger/" + id, EmptySuccessResponse.class);
        if (response instanceof EmptySuccessResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            return (EmptySuccessResponse) response;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected EditPassengerResponse editPassengerById(int id, String firstName, String lastName, String patronymic, String dateOfBirth, ErrorCode expectedStatus) {
        EditPassengerRequest request = new EditPassengerRequest(firstName, lastName, patronymic, dateOfBirth);
        Object response = client.put(baseURL + "/passenger/" + id, request, EditPassengerResponse.class);
        if (response instanceof EditPassengerResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            EditPassengerResponse editPassengerResponse = (EditPassengerResponse) response;
            checkEditPassengerResponse(editPassengerResponse, firstName, lastName, patronymic, dateOfBirth);
            return editPassengerResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected GetPassengerByIdResponse getPassengerById(int id, String firstName, String lastName, String
            patronymic , String dateOfBirth, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/passengers/" + id, GetPassengerByIdResponse.class);
        if (response instanceof GetPassengerByIdResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            GetPassengerByIdResponse getPassengerByIdResponse = (GetPassengerByIdResponse) response;
            assertEquals(id, getPassengerByIdResponse.getPassengerId());
            checkGetPassengerByIdResponse(getPassengerByIdResponse, firstName, lastName, patronymic, dateOfBirth);
            return getPassengerByIdResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected List<GetPassengerResponce> getPlaneToPassengerById(int id, int expectedCount, ErrorCode expectedStatus) {
        Object response = client.get(baseURL + "/passenger/" + id, List.class);
        if (response instanceof List<?>) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            @SuppressWarnings("unchecked")
            List<GetPassengerResponce> getPlneResponse = (List<GetPassengerResponce>) response;
            assertEquals(expectedCount, getPlneResponse.size());
            return getPlneResponse;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }


}

