package ru.omsu.imit.base;

import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import ru.omsu.imit.dao.*;
import ru.omsu.imit.daoimpl.*;
import ru.omsu.imit.model.Passenger;
import ru.omsu.imit.model.Plane;
import ru.omsu.imit.utils.MyBatisUtils;

import java.util.Set;

import static org.junit.Assert.assertEquals;

public class BaseDaoTest {
    protected CommonDao commonDao = new CommonDaoImpl();
    protected PlaneDao planeDao = new PlaneDaoImpl();
    protected FlightDao flightDao = new FlightDaoImpl();
    protected TicketDao ticketDao = new TicketDaoImpl();
    protected PassengerDao passengerDao = new PassengerDaoImpl();

    @BeforeClass()
    public static void init() {
        Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());

    }

    @Before
    public void insertData() {
        planeDao.insert(new Plane(1,"Airbus", "20"));
        planeDao.insert(new Plane(2, "Beechcraft","40"));
        planeDao.insert(new Plane(3, "Boeing","150"));
        planeDao.insert(new Plane(4, "Bombardier Aerospace","10"));
        planeDao.insert(new Plane(5, "Cessna Aircraft","16"));
    }

    @Before()
    public void clearDatabase() {
        ticketDao.deleteAll();
        passengerDao.deleteAll();
        flightDao.deleteAllFlight();
        planeDao.deleteAll();
    }


    protected void checkPlaneFields(Plane plane1, Plane plane2) {
        assertEquals(plane1.getName(), plane2.getName());
        assertEquals(plane1.getCapacity(), plane2.getCapacity());
    }

    protected void checkPassengerFields(Passenger passenger1, Passenger passenger2) {
        assertEquals(passenger1.getId(), passenger2.getId());
        assertEquals(passenger1.getDateOfBirth(), passenger2.getDateOfBirth());
        assertEquals(passenger1.getFirstName(), passenger2.getFirstName());
    }

}
