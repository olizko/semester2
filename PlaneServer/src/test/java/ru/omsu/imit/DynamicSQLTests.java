package ru.omsu.imit;

import org.junit.Test;
import ru.omsu.imit.base.BaseDaoTest;
import ru.omsu.imit.exception.AirplaneException;
import ru.omsu.imit.model.Plane;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class DynamicSQLTests extends BaseDaoTest {
    @Test
    public void testDelete() throws AirplaneException {

        Plane plane1 = new Plane("Cy-100", "100");
        Plane plane2 = new Plane("Cy-27", "2");
        Plane plane3 = new Plane("Airbus-A318", "132");

        planeDao.insert(plane1);
        planeDao.insert(plane2);
        planeDao.insert(plane3);
        planeDao.delete(plane1);

        List<Plane> planes = planeDao.getAllLazy();
        assertEquals(planes.size(), 7);

    }


    @Test
    public void testGetPlaneById() throws AirplaneException {
        Plane plane1 = new Plane("Cy-100", "100");
        planeDao.insert(plane1);
        planeDao.getById(plane1.getId());

        Plane changedPlane = planeDao.getById(plane1.getId());


    }


}
