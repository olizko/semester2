package ru.omsu.imit.passenger;

import org.junit.Test;
import ru.omsu.imit.base.BaseDaoTest;
import ru.omsu.imit.exception.AirplaneException;
import ru.omsu.imit.model.Flight;
import ru.omsu.imit.model.Passenger;
import ru.omsu.imit.model.Plane;
import ru.omsu.imit.model.Ticket;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.*;

public class PassengerTest extends BaseDaoTest {

    @Test
    public void testGetPassenger() {
        Plane plane = new Plane("Airbus","120");
        planeDao.insert(plane);
        Flight flight = new Flight("Moscow","Omsk", LocalDate.of(2018,9,19), LocalTime.of(12,55), LocalTime.of(15,55),plane);
        flightDao.insertFlight(flight);
        Passenger passenger1 = new Passenger("Иван", "Иванов", "Иванович", LocalDate.of(1995, 5, 1));
        passengerDao.insert(passenger1);
        Ticket ticket = new Ticket(2,"business", passenger1 , flight);
        ticketDao.insert(ticket);
    }

    @Test
    public void testGetPassengerWithTwoTicket() {
        Plane plane = new Plane("Airbus","120");
        planeDao.insert(plane);
        Flight flight = new Flight("Moscow","Omsk", LocalDate.of(2018,9,19), LocalTime.of(12,55), LocalTime.of(15,55),plane);
        flightDao.insertFlight(flight);
        Passenger passenger = new Passenger("Иван", "Иванов", "Иванович", LocalDate.of(1995, 5, 1));
        passengerDao.insert(passenger);
        Ticket ticketOne = new Ticket(2,"business", passenger , flight);
        Ticket ticketTwo = new Ticket(4,"business", passenger , flight);
        ticketDao.insert(ticketOne);
        ticketDao.insert(ticketTwo);

        Passenger byId = passengerDao.getById(passenger.getId());
        assertEquals(2, byId.getTickets().size());
    }


    @Test
    public void testDeletePassenger() {
        Passenger passenger1 = new Passenger("Иван", "Иванов", "Иванович", LocalDate.of(1995, 5, 1));

        passengerDao.insert(passenger1);
        passengerDao.deleteAll();
        assertNotEquals(passengerDao.getById(passenger1.getId()), passenger1.getId());
    }

    @Test
    public void testDeletePassengerById() throws AirplaneException {
        Passenger passenger1 = new Passenger("Иван", "Иванов", "Иванович", LocalDate.of(1995, 5, 1));
        passengerDao.insert(passenger1);
        passengerDao.deletePassengerById(passenger1.getId());
        Passenger passenger = passengerDao.getById(passenger1.getId());
        assertNull(passenger);
    }

    @Test
    public void testGetById() throws AirplaneException {
        Passenger passenger1 = new Passenger("Иван", "Иванов", "Иванович", LocalDate.of(1995, 5, 1));
        Passenger passenger2 = new Passenger("Пётр", "Петров", "Иванович", LocalDate.of(1991, 3, 7));

        passengerDao.insert(passenger1);
        passengerDao.insert(passenger2);
        Passenger passenger = passengerDao.getById(passenger1.getId());
        assertEquals(passenger.getFirstName(), passenger1.getFirstName());

    }

    @Test
    public void testDeletePasssengers() throws AirplaneException {
        Passenger passenger = new Passenger("Иван", "Иванов", "Иванович", LocalDate.of(1995, 5, 1));
        passengerDao.insert(passenger);
        Passenger passengerFromDb = passengerDao.getById(passenger.getId());
        checkPassengerFields(passenger, passengerFromDb);
        passengerDao.deletePassenger(passenger);
        passengerFromDb = passengerDao.getById(passenger.getId());
        assertNull(passengerFromDb);
    }

    @Test
    public void testInsertPassenger() {
        Passenger passenger = new Passenger("Иван", "Иванов", "Иванович", LocalDate.of(1995, 5, 1));
        passengerDao.insert(passenger);
        Passenger passenger1 = passengerDao.getById(passenger.getId());
        assertEquals(passenger.getFirstName(), passenger1.getFirstName());
    }

    @Test
    public void testDeleteAllPassenger() {
        Passenger passenger1 = new Passenger("Иван", "Иванов", "Иванович", LocalDate.of(1995, 5, 1));
        Passenger passenger2 = new Passenger("Пётр", "Иванов", "Иванович", LocalDate.of(1995, 5, 1));
        Passenger passenger3 = new Passenger("Василий", "Иванов", "Иванович", LocalDate.of(1995, 5, 1));
        passengerDao.insert(passenger1);
        passengerDao.insert(passenger2);
        passengerDao.insert(passenger3);
        passengerDao.deleteAll();
        Passenger passenger = passengerDao.getById(passenger1.getId());
        assertNull(passenger);
    }

    @Test
    public void testGetPassengerById() {
        Passenger passenger1 = new Passenger("Иван", "Иванов", "Иванович", LocalDate.of(1995, 5, 1));
        Passenger passenger2 = new Passenger("Пётр", "Иванов", "Иванович", LocalDate.of(1995, 5, 1));
        passengerDao.insert(passenger1);
        passengerDao.insert(passenger2);
        Passenger passenger = passengerDao.getById(passenger1.getId());
        assertEquals(passenger1.getFirstName(), passenger.getFirstName());
    }

    @Test
    public void testEditPassengerById() {
        Passenger passenger1 = new Passenger("Иван", "Иванов", "Иванович", LocalDate.of(1995, 5, 1));
        Passenger passenger2 = new Passenger("Пётр", "Иванов", "Иванович", LocalDate.of(1995, 5, 1));
        passengerDao.insert(passenger1);
        passengerDao.editPassengerById(passenger1.getId(), passenger2);
        Passenger passenger = passengerDao.getById(passenger1.getId());
        assertEquals(passenger.getFirstName(), passenger2.getFirstName());
        passengerDao.deleteAll();
    }

}