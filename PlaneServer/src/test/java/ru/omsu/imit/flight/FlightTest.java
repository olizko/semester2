package ru.omsu.imit.flight;

import org.junit.Test;
import ru.omsu.imit.base.BaseDaoTest;
import ru.omsu.imit.model.Flight;
import ru.omsu.imit.model.Plane;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.Assert.assertNull;
import static  org.junit.Assert.assertEquals;

public class FlightTest extends BaseDaoTest {

    @Test
    public void testInsertFlight() {
        Plane plane = new Plane("Cy-100","100");
        Flight flight = new Flight("Moscow","Omsk", LocalDate.of(2018,9,19), LocalTime.of(12,55), LocalTime.of(15,55),plane);
        planeDao.insert(plane);
        flightDao.insertFlight(flight);
    }

    @Test
    public void testGetFlightById() {
        Plane plane = new Plane("Cy-100","100");
        Flight flight = new Flight("Moscow","Omsk", LocalDate.of(2018,9,19), LocalTime.of(12,55), LocalTime.of(15,55),plane);
        planeDao.insert(plane);
        flightDao.insertFlight(flight);
        Flight flight1 = flightDao.getFlightById(flight.getId());
        assertEquals(flight.getTimeDepartment(), flight1.getTimeDepartment());
        assertEquals(flight.getFlightFrom(), flight1.getFlightFrom());
        assertEquals(flight.getTimeArrival(), flight1.getTimeArrival());
        assertEquals(flight.getFlightTo(), flight1.getFlightTo());
    }

    @Test
    public void testEditFlightById() {

        Plane plane1 = new Plane("Cy-100","100" );
        Plane plane2 = new Plane("Cy-27", "2");
        Flight flight1 = new Flight("Moscow","Omsk", LocalDate.of(2018,9,19), LocalTime.of(12,55), LocalTime.of(15,55),plane1);
        Flight flight2 = new Flight("Moscow","Sochi", LocalDate.of(2018,10,12), LocalTime.of(13,45), LocalTime.of(16,10),plane2);
        planeDao.insert(plane1);
        planeDao.insert(plane2);
        flightDao.insertFlight(flight1);
        flightDao.insertFlight(flight2);
        flightDao.editFlight(flight1.getId(), flight2);
        Flight flight = flightDao.getFlightById(flight1.getId());
        assertEquals(flight.getFlightTo(), flight2.getFlightTo());
    }

    @Test
    public void testDeleteFlightById() {
        Plane plane1 = new Plane("Cy-100","100");
        Plane plane2 = new Plane("Cy-27", "2");
        Flight flight1 = new Flight("Moscow","Omsk", LocalDate.of(2018,9,19), LocalTime.of(12,55), LocalTime.of(15,55),plane1);
        Flight flight2 = new Flight("Moscow","Sochi", LocalDate.of(2018,10,12), LocalTime.of(13,45), LocalTime.of(16,10),plane2);
        planeDao.insert(plane1);
        planeDao.insert(plane2);
        flightDao.insertFlight(flight1);
        flightDao.insertFlight(flight2);
        flightDao.deleteFlightById(flight1.getId());
        flightDao.deleteFlightById(flight2.getId());
        Flight flight = flightDao.getFlightById(flight1.getId());
        assertNull(flight);
    }

    @Test
    public void testDeleteAllFlight() {
        Plane plane1 = new Plane("Cy-100","100");
        Plane plane2 = new Plane("Cy-27", "2");
        Flight flight1 = new Flight("Moscow","Omsk", LocalDate.of(2018,9,19), LocalTime.of(12,55), LocalTime.of(15,55),plane1);
        Flight flight2 = new Flight("Moscow","Sochi", LocalDate.of(2018,10,12), LocalTime.of(13,45), LocalTime.of(16,10),plane2);
        planeDao.insert(plane1);
        planeDao.insert(plane2);
        flightDao.insertFlight(flight1);
        flightDao.insertFlight(flight2);
        flightDao.deleteAllFlight();
        Flight flight = flightDao.getFlightById(flight1.getId());
        assertNull(flight);
    }
}
