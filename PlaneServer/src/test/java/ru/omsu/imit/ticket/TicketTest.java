package ru.omsu.imit.ticket;

import org.junit.Ignore;
import org.junit.Test;
import ru.omsu.imit.base.BaseDaoTest;
import ru.omsu.imit.exception.AirplaneException;
import ru.omsu.imit.model.Flight;
import ru.omsu.imit.model.Passenger;
import ru.omsu.imit.model.Plane;
import ru.omsu.imit.model.Ticket;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TicketTest extends BaseDaoTest {
    @Test
    public void testAddPassengerToPlane() {
        Plane plane = new Plane("Cy-100","100");
        Passenger passenger = new Passenger("Иван", "Иванов", "Иванович", LocalDate.of(1995, 5, 1));
        planeDao.insert(plane);
        passengerDao.insert(passenger);
        Flight flight = new Flight("Moscow","Omsk", LocalDate.of(2018,9,19), LocalTime.of(12,55), LocalTime.of(15,55),plane);
        flightDao.insertFlight(flight);
        Ticket ticket = new Ticket(2,"business", passenger , flight);
        ticketDao.insert(ticket);

        assertEquals(ticket.getPassenger(), passenger);
        assertEquals(flight.getPlane(), plane);
    }


    @Test
    public void testDeleteAllTicket() {
        Plane plane = new Plane("Cy-100","100");
        planeDao.insert(plane);

        Passenger passenger1 = new Passenger("Иван", "Иванов", "Иванович", LocalDate.of(1995, 5, 1));
        Passenger passenger2 = new Passenger("Пётр", "Иванов", "Иванович", LocalDate.of(1995, 5, 1));
        Passenger passenger3 = new Passenger("Василий", "Иванов", "Иванович", LocalDate.of(1995, 5, 1));

        Flight flight = new Flight("Moscow","Omsk", LocalDate.of(2018,9,19), LocalTime.of(12,55), LocalTime.of(15,55),plane);
        flightDao.insertFlight(flight);

        Ticket ticket1 = new Ticket(2,"business", passenger1 , flight);
        Ticket ticket2 = new Ticket(5,"economy", passenger2 , flight);
        Ticket ticket3 = new Ticket(15,"economy", passenger3 , flight);

        passengerDao.insert(passenger1);
        passengerDao.insert(passenger2);
        passengerDao.insert(passenger3);

        ticketDao.insert(ticket1);
        ticketDao.insert(ticket2);
        ticketDao.insert(ticket3);
        ticketDao.deleteAll();

        List<Ticket> tickets = ticketDao.getAllTicketByFlightId(flight.getId());
        assertEquals(0, tickets.size());
    }

    @Test
    public void testDeleteTicketById() throws AirplaneException {
        Plane plane1 = new Plane("Cy-100","100" );
        planeDao.insert(plane1);

        Passenger passenger1 = new Passenger("Иван", "Иванов", "Иванович", LocalDate.of(1995, 5, 1));
        passengerDao.insert(passenger1);

        Flight flight1 = new Flight("Moscow","Omsk", LocalDate.of(2018,9,19), LocalTime.of(12,55), LocalTime.of(15,55), plane1);
        flightDao.insertFlight(flight1);

        Ticket ticket1 = new Ticket(2,"business", passenger1 , flight1);
        ticketDao.insert(ticket1);

        ticketDao.deleteTicketById(ticket1.getId());
        Ticket ticket = ticketDao.getById(ticket1.getId());
        assertNull(ticket);
    }
}
