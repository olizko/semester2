package ru.omsu.imit.resources;

import org.junit.Test;
import ru.omsu.imit.base.BasePassengerClientTest;
import ru.omsu.imit.exception.AirplaneException;
import ru.omsu.imit.rest.response.passenger.PassengerResponse;
import ru.omsu.imit.utils.ErrorCode;

public class TestPassengerResource extends BasePassengerClientTest {
    @Test
    public void testAddPassenger() throws AirplaneException {
        addPassenger("Иван", "Иванов", "Иванович", "1996-01-01", ErrorCode.SUCCESS);
    }

    @Test
    public void testDeletePassengerId() {
        PassengerResponse addResponse = addPassenger("Иван", "Иванов", "Иванович", "1996-01-01", ErrorCode.SUCCESS);
        deletePassengerById(addResponse.getId(), ErrorCode.SUCCESS);
    }

    @Test
    public void testEditById() {
        PassengerResponse addResponse = addPassenger("Иван", "Иванов", "Иванович", "1996-01-01", ErrorCode.SUCCESS);
        editPassengerById(addResponse.getId(), "Иван", "Иванов", "Иванович", "1996-01-01", ErrorCode.SUCCESS);
    }

    @Test
    public void testGetById() {
        PassengerResponse addResponse = addPassenger("Иван", "Иванов", "Иванович", "1996-01-01", ErrorCode.SUCCESS);
        getPassengerById(addResponse.getId(), "Иван", "Иванов", "Иванович", "1996-01-01", ErrorCode.SUCCESS);
    }
}
