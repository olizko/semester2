package ru.omsu.imit.resources;

import org.junit.Test;
import ru.omsu.imit.exception.AirplaneException;
import ru.omsu.imit.base.BasePlaneClientTest;
import ru.omsu.imit.rest.response.plane.PlaneResponse;
import ru.omsu.imit.utils.ErrorCode;

import java.time.LocalDate;

public class TestPlaneResource extends BasePlaneClientTest {


    @Test
    public void testAddPlane() throws AirplaneException {
        addPlane("Cy-100", "100",ErrorCode.SUCCESS);
    }

    @Test
    public void testDeletePlaneById() {
        PlaneResponse addResponse = addPlane("Cy-100", "100",ErrorCode.SUCCESS);
        deletePlaneById(addResponse.getPlaneId(), ErrorCode.SUCCESS);
    }

    @Test
    public void testEditById() {
        PlaneResponse addResponse = addPlane("Cy-100", "100",ErrorCode.SUCCESS);
        editPlaneById(addResponse.getPlaneId(), "Cy-150", "123",ErrorCode.SUCCESS);
    }

    @Test
    public void testGetById() {
        PlaneResponse addResponse = addPlane("Cy-100", "100",ErrorCode.SUCCESS);
        getPlaneById(addResponse.getPlaneId(),"Cy-100", "100",ErrorCode.SUCCESS);
    }

}
