package ru.omsu.imit.resources;

import org.junit.Test;
import ru.omsu.imit.base.BaseFlightTest;
import ru.omsu.imit.rest.response.flight.AddFlightResponse;
import ru.omsu.imit.rest.response.plane.PlaneResponse;
import ru.omsu.imit.utils.ErrorCode;

public class TestFlightResource extends BaseFlightTest {

    @Test
    public void testAddFlight(){
        PlaneResponse planeResponse = addPlane("Су-100","100", ErrorCode.SUCCESS);
        addFlight("Omsk", "Moskow", "2018-02-02","09:00","13:00",planeResponse.getPlaneId(), ErrorCode.SUCCESS) ;
    }

    @Test
    public  void testGetById(){
        PlaneResponse planeResponse = addPlane("Су-100","100", ErrorCode.SUCCESS);
        AddFlightResponse flightResponse = addFlight("Omsk", "Moskow", "2018-02-02","09:00","13:00",planeResponse.getPlaneId(), ErrorCode.SUCCESS);
        getFlightById(flightResponse.getFlightId(),"Omsk", "Moskow", "2018-02-02","09:00","13:00",planeResponse.getPlaneId(), ErrorCode.SUCCESS);
    }
}
