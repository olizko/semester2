package ru.omsu.imit.resources;

import org.junit.Test;
import ru.omsu.imit.base.BaseTicketTest;
import ru.omsu.imit.rest.response.flight.AddFlightResponse;
import ru.omsu.imit.rest.response.passenger.PassengerResponse;
import ru.omsu.imit.rest.response.plane.PlaneResponse;
import ru.omsu.imit.utils.ErrorCode;

public class TestTicketResource extends BaseTicketTest {

    @Test
    public void testAddPassengerToFlight() {
        PlaneResponse plane = addPlane("Cy-100", "100", ErrorCode.SUCCESS);
        AddFlightResponse flightResponse = addFlight("Omsk", "Moskow", "2018-02-02", "09:00", "13:00", plane.getPlaneId(), ErrorCode.SUCCESS);
        PassengerResponse passengerOne = addPassenger("Иван", "Иванов", "Иванович", "1996-01-01", ErrorCode.SUCCESS);
        PassengerResponse passengerTwo = addPassenger("Вася", "Пупкин", "Пупкинович", "1999-05-05", ErrorCode.SUCCESS);

        addPassengerToPlaneResponce(2, "economy", flightResponse.getFlightId(), passengerOne.getId(), ErrorCode.SUCCESS);
        addPassengerToPlaneResponce(5, "economy", flightResponse.getFlightId(), passengerTwo.getId(), ErrorCode.SUCCESS);
        getFlightToPassengerById(flightResponse.getFlightId(), 2, ErrorCode.SUCCESS);
    }

    @Test
    public void testAddPassengerToPlaneWrongPassenger() {
        PlaneResponse plane = addPlane("Cy-100", "100", ErrorCode.SUCCESS);
        AddFlightResponse flightResponse = addFlight("Omsk", "Moskow", "2018-02-02", "09:00", "13:00", plane.getPlaneId(), ErrorCode.SUCCESS);
        PassengerResponse passenger = addPassenger("Вася", "Пупкин", "Пупкинович", "1999-05-05", ErrorCode.SUCCESS);

        addPassengerToPlaneResponce(5, "economy", flightResponse.getFlightId(), passenger.getId() + 5, ErrorCode.PASSENGER_NOT_FOUND);
    }

}
