-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mydb` ;

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`plane`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`plane` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `capacity` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 166
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`flight`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`flight` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `flightFrom` VARCHAR(45) NULL DEFAULT NULL,
  `flightTo` VARCHAR(45) NULL DEFAULT NULL,
  `date` DATE NULL DEFAULT NULL,
  `timeArrival` TIME NULL DEFAULT NULL,
  `timeDepartment` TIME NULL,
  `planeId` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_flight_plane1_idx` (`planeId` ASC),
  CONSTRAINT `fk_flight_plane1`
    FOREIGN KEY (`planeId`)
    REFERENCES `mydb`.`plane` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 39
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`passenger`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`passenger` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(45) NULL DEFAULT NULL,
  `lastname` VARCHAR(45) NULL DEFAULT NULL,
  `patronymic` VARCHAR(45) NULL DEFAULT NULL,
  `dateOfBirth` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 117
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`ticket`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`ticket` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `seat` VARCHAR(45) NULL DEFAULT NULL,
  `clazz` VARCHAR(45) NULL DEFAULT NULL,
  `passengerId` INT(11) NOT NULL,
  `flightId` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Ticket_Passenger1_idx` (`passengerId` ASC),
  INDEX `fk_ticket_flight1_idx` (`flightId` ASC),
  CONSTRAINT `fk_Ticket_Passenger1`
    FOREIGN KEY (`passengerId`)
    REFERENCES `mydb`.`passenger` (`id`),
  CONSTRAINT `fk_ticket_flight1`
    FOREIGN KEY (`flightId`)
    REFERENCES `mydb`.`flight` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 41
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;



START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`plane` (`id`, `name`, `capacity`) VALUES (1, 'Airbus ', 20);
INSERT INTO `mydb`.`plane` (`id`, `name`, `capacity`) VALUES (2, 'Beechcraft', 40);
INSERT INTO `mydb`.`plane` (`id`, `name`, `capacity`) VALUES (3, 'Boeing', 150);
INSERT INTO `mydb`.`plane` (`id`, `name`, `capacity`) VALUES (4, 'Bombardier Aerospace', 10);
INSERT INTO `mydb`.`plane` (`id`, `name`, `capacity`) VALUES (5, 'Cessna Aircraft', 16);

COMMIT;