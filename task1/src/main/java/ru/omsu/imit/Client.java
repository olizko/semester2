package ru.omsu.imit;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {

    private static final Logger logger = Logger.getLogger("Client");

    public static void main(String[] args) {

        InetAddress ipAddress;
        try {
            ipAddress = InetAddress.getByName("localhost");
        } catch (UnknownHostException e) {
            logger.log(Level.SEVERE, "UnknownHostExcpetion.");
            return;
        }

        try(Socket socket = new Socket(ipAddress, 6666);
            DataInputStream dis = new DataInputStream(socket.getInputStream());
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            BufferedReader keyboardReader = new BufferedReader(new InputStreamReader(System.in, "CP866"))){

            System.out.println("Connected to server on " + socket.toString());
            System.out.println("Server: " + dis.readUTF());

            while(true){

                System.out.print("Me: ");
                String line = keyboardReader.readLine();
                if (line.equalsIgnoreCase("/stopclient")) {
                    System.out.println("Stopping client.");
                    break;
                }
                System.out.println("Sending message to the server...");
                dos.writeUTF(line);
                dos.flush();
                line = dis.readUTF();
                System.out.println("Server: " + line);
                if (line.equalsIgnoreCase("Goodbye.")) {
                    System.out.println("Stopping client.");
                    break;
                }

            }

        }catch(IOException e){
            logger.log(Level.SEVERE, "IOException!");
            return;
        }

    }

}
