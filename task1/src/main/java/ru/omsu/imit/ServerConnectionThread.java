package ru.omsu.imit;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerConnectionThread extends Thread{

    private Socket clientSocket;
    private int clientID;
    private final Logger logger;

    public ServerConnectionThread(Socket clientSocket, int clientID) {
        this.clientSocket = clientSocket;
        this.clientID = clientID;
        this.logger = Logger.getLogger(clientID + " clientConnection");
    }

    @Override
    public void run() {
        try (DataInputStream dis = new DataInputStream(clientSocket.getInputStream());
             DataOutputStream dos = new DataOutputStream(clientSocket.getOutputStream())){

            logger.info("Hello! I'm the new connection thread! :). Client " + clientID + " connected succesfully.");

            dos.writeUTF("Hello! Type 'help' to view the commands. ");
            dos.flush();

            while(true) {
                String clientMessage = dis.readUTF();
                logger.info("Client " + clientID + " sent: " + clientMessage);

                if (clientMessage.equalsIgnoreCase("help")) {
                    dos.writeUTF("\n" +
                                     "'stop' - stop the connection.\n" +
                                     "'game' - play the game.\n" +
                                     "'help' - view available commands.");

                } else if (clientMessage.equalsIgnoreCase("game")){
                    int serverNumber = new Random().nextInt(1000);
                    int triesCount = 0;
                    dos.writeUTF("Game has been started! Try to guess the number from 0 to 1000");
                    dos.flush();
                    while(true){
                        if(triesCount > 4){
                            break;
                        }
                        int clientNumber = Integer.parseInt(dis.readUTF());
                        while (clientNumber < 0 || clientNumber > 1000){
                            dos.writeUTF("Enter the integer number from 0 to 1000");
                            dos.flush();
                            clientNumber = Integer.parseInt(dis.readUTF());
                        }
                        triesCount++;
                        if (clientNumber < serverNumber){
                            dos.writeUTF("i have greater.");
                            dos.flush();
                        } else if (clientNumber > serverNumber){
                            dos.writeUTF("i have lesser");
                            dos.flush();
                        } else {
                            break;
                        }
                    }
                    if (triesCount <= 4) {
                        dos.writeUTF("Guessed! It's " + serverNumber + ". Count of your tries: " + triesCount + ". ");
                        dos.flush();
                        logger.info("Client " + clientID + " won the game with " + triesCount + " tries.");
                    }else{
                        dos.writeUTF("Count of your tries is more than 5. Game over.");
                        dos.flush();
                        logger.info("Client " + clientID + " lose the game.");
                    }

                } else if (clientMessage.equalsIgnoreCase("stop")){
                    dos.writeUTF("Goodbye.");
                    break;

                } else {
                    dos.writeUTF("I'm sorry, but i don't understand you. Type 'help' to view the commands.");
                }

                dos.flush();
            }

        }catch (IOException e){
            logger.log(Level.SEVERE, "IOException.");
        }finally {
            logger.info("Client " + clientID + " disconnected from " + clientSocket.toString() + ". Closing socket...");
            try{
                clientSocket.close();
                logger.info(clientSocket.toString() + " has been closed succesfully.");
            }catch(IOException e){
                logger.log(Level.SEVERE, "IOException while closing " + clientSocket.toString() + ". ");
            }
        }
    }
}