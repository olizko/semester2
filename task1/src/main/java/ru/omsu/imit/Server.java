package ru.omsu.imit;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server {

    private final static Logger logger = Logger.getLogger("Server");

    public static void main(String[] args){

        ExecutorService executorService = Executors.newCachedThreadPool();
        try (ServerSocket serverSocket = new ServerSocket(6666)){
            logger.info("Server is ready.");
            int id = 0;
            while (true) {
                Socket clientSocket = serverSocket.accept();
                id++;
                logger.info("New client with id '" + id + "' trying to connect on " + clientSocket.toString() + ". Creating new connection thread...");
                executorService.execute(new ServerConnectionThread(clientSocket, id));
            }
        }catch (IOException e){
            logger.log(Level.SEVERE, "Input/output exception.");
        }

    }

}
