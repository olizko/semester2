package ru.omsu.imit;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class BooksBaseOps {

    public static Collection<Book> getAll(Connection con) {
        String query = "select * from books";
        try (Statement stmt = con.createStatement();) {
            ResultSet rs = stmt.executeQuery(query);
            Collection<Book> res = new ArrayList<Book>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                res.add(new Book(id, title, year, pages));
            }
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Collection<Book> getNFirst(Connection con, int count) {
        String query =  "select * from books where id < ? ";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1, count);
            ResultSet rs = stmt.executeQuery();
            Collection<Book> res = new ArrayList<Book>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                res.add(new Book(id, title, year, pages));
            }
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Collection<Book> getById(Connection con, int sId) {
        String query =  "select * from books where id = ? ";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1, sId);
            ResultSet rs = stmt.executeQuery();
            Collection<Book> res = new ArrayList<Book>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                res.add(new Book(id, title, year, pages));
            }
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Collection<Book> getByTitle(Connection con, String needtitle) {
        String query =  "select * from books where title = ? ";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setString(1,needtitle);
            ResultSet rs = stmt.executeQuery();
            Collection<Book> res = new ArrayList<Book>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                res.add(new Book(id, title, year, pages));
            }
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Collection<Book> getByFirstWord(Connection con, String str) {
        String query =  "select * from books where title like ? ";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setString(1, str+"%");
            ResultSet rs = stmt.executeQuery();
            Collection<Book> res = new ArrayList<Book>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                res.add(new Book(id, title, year, pages));
            }
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Collection<Book> getByLastWord(Connection con, String str) {
        String query =  "select * from books where title like ? ";
        try (PreparedStatement stmt = con.prepareStatement(query);) {
            stmt.setString(1, "%"+str);
            ResultSet rs = stmt.executeQuery();
            Collection<Book> res = new ArrayList<Book>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                res.add(new Book(id, title, year, pages));
            }
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Collection<Book> getByYearBetween(Connection con, int left, int right) {
        String query = "select * from books where year between ? and ? " ;
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1, left);
            stmt.setInt(2, right);
            ResultSet rs = stmt.executeQuery();
            Collection<Book> res = new ArrayList<Book>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                res.add(new Book(id, title, year, pages));
            }
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Collection<Book> getByPagesCountBetween(Connection con, int left, int right) {
        String query = "select * from books where pages between ? and ?";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1,left);
            stmt.setInt(2,right);
            ResultSet rs = stmt.executeQuery();
            Collection<Book> res = new ArrayList<Book>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                res.add(new Book(id, title, year, pages));
            }
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Collection<Book> getByPagesCountBetweenAndYearBetween(Connection con, int leftCount, int rightCount, int leftYear, int rightYear) {
        String query = "select * from books where pages between ? and ? AND year between ? and ?";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1, leftCount);
            stmt.setInt(2, rightCount);
            stmt.setInt(3, leftYear);
            stmt.setInt(4, rightYear);
            stmt.executeQuery();
            ResultSet rs = stmt.getResultSet();
            Collection<Book> res = new ArrayList<Book>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                int year = rs.getInt("year");
                int pages = rs.getInt("pages");
                res.add(new Book(id, title, year, pages));
            }
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void setPagesForBooksExampleOne(Connection con, int p){
        //установить для всех книг количество страниц, равное P
        String query = "update books set pages = ?";
        try (PreparedStatement stmt = con.prepareStatement(query)){
            stmt.setInt(1, p);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setPagesForBooksExampleTwo(Connection con, int pSet, int pCondition){
        //установить количество страниц, равное P1 для всех книг , количество страниц которых больше P2
        String query = "update books set pages = ? where pages > ?" ;
        try (PreparedStatement stmt = con.prepareStatement(query)){
            stmt.setInt(1, pSet);
            stmt.setInt(2, pCondition);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setPagesForBooksExampleThree(Connection con, int pSet, int pCondition, int yCondition){
        //установить количество страниц, равное P1 для всех книг , количество страниц которых больше P2 и год выпуска раньше Y
        String query = "update books set pages = ? where pages > ? and year < ?" ;
        try (PreparedStatement stmt = con.prepareStatement(query)){
            stmt.setInt(1, pSet);
            stmt.setInt(2, pCondition);
            stmt.setInt(3, yCondition);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setPagesForBooksExampleFour(Connection con, int pSet, int pCondition, String wCondition){
        //установить количество страниц, равное P1 для всех книг , количество страниц которых больше P2 и название начинается со слова W
        String query = "update books set pages = ? where pages > ? and title like ? " ;
        try (PreparedStatement stmt = con.prepareStatement(query)){
            stmt.setInt(1, pSet);
            stmt.setInt(2, pCondition);
            stmt.setString(3, wCondition + "%");
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteBooksExampleOne(Connection con, int p){
        //Удалить записи о книгах количество страниц которых больше P
        String query = "delete from books where pages > ?";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1, p);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteBooksExampleTwo(Connection con, String str){
        //Удалить записи о книгах название которых начинается со слова str
        String query = "delete from books where title like ?";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setString(1, str+"%");
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteBooksExampleThree(Connection con, int idLeft, int idRight){
        //Удалить записи о книгах, id которых > id1 и < id2.
        String query = "delete from books where id > ? and id < ?";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setInt(1, idLeft);
            stmt.setInt(2, idRight);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteBooksExampleFour(Connection con, String title){
        //Удалить записи о книгах с названием N.
        String query = "delete from books where title = ?";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setString(1, title);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteBooksExampleFive(Connection con, String titleOne, String titleTwo, String titleThree){
        //Удалить записи о книгах с названиями N1, N2, N3.
        String query = "delete from books where title = ? or title = ? or title = ? ";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setString(1, titleOne);
            stmt.setString(2, titleTwo);
            stmt.setString(3, titleThree);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteBooksExampleSix(Connection con){
        //Удалить записи о книгах с названиями N1, N2, N3.
        String query = "delete from books";
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addNewColumn(Connection con, String columnName, String dataType){
        String query = "alter table books add "+ columnName + " " + dataType +";";
        try(PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addBindingEnum(Connection con){
        String query = "alter table books add binding enum('without binding', 'soft', 'hard')";
        try(PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setEnumForAll(Connection con){
        String query = "update books set binding = 1";
        try (Statement stmt = con.createStatement()){
            stmt.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setPublishingById(Connection con, String value, int idLeft, int idRight){
        String query = "update BOOKS set publishing_house = ? where id > ? and id < ? ";
        try (PreparedStatement stmt = con.prepareStatement(query)){
            stmt.setString(1, value);
            stmt.setInt(2, idLeft);
            stmt.setInt(3, idRight);
            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void changeColumnName(Connection con, String lastName, String newName){
        String query = "alter table books change column " + lastName + " " + newName+ " VARCHAR(45)";
        try (Statement stmt = con.createStatement()){
            stmt.execute(query);
        } catch (SQLException e) {
           e.printStackTrace();
        }
    }

    public static void renameTable(Connection con, String lastName, String newName){
        String query = "alter table " + lastName + " rename " + newName;
        try (Statement stmt = con.createStatement()){
            stmt.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteTable(Connection con, String tableName){
        String query = "drop table " + tableName;
        try (Statement stmt = con.createStatement()){
            stmt.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}