package ru.omsu.imit;

import org.junit.Test;
import java.sql.*;
import java.util.Collection;

public class BooksBaseOpsTest {

    //упражнение 3: javaПриложение <-(jdbc)-----> SQLServer <------> schemaBooksDataBase.

    private static final String URL = "jdbc:mysql://localhost:3306/booksdatabase";
    private static final String USER = "root";
    private static final String PASSWORD = "9293709b13V";

    private static boolean loadDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return true;
        } catch (ClassNotFoundException e) {
            System.out.println("Error loading JDBC Driver ");
            e.printStackTrace();
            return false;
        }
    }

    @Test
    public void getAllTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            Collection<Book> result = BooksBaseOps.getAll(con);
            for (Book item : result) {
                System.out.println(item);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void getNFirstTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            Collection<Book> result = BooksBaseOps.getNFirst(con,3);
            for (Book item : result) {
                System.out.println(item);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void getByIdTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            Collection<Book> result = BooksBaseOps.getById(con,2);
            for (Book item : result) {
                System.out.println(item);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void getByTitleTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            Collection<Book> result = BooksBaseOps.getByTitle(con, "kolobok");
            for (Book item : result) {
                System.out.println(item);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void getByFirstWordTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            Collection<Book> result = BooksBaseOps.getByFirstWord(con, "kolo");
            for (Book item : result) {
                System.out.println(item);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void getByLastWordTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            Collection<Book> result = BooksBaseOps.getByLastWord(con, "zka");
            for (Book item : result) {
                System.out.println(item);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void getByYearBetweenTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            Collection<Book> result = BooksBaseOps.getByYearBetween(con, 2002, 2003);
            for (Book item : result) {
                System.out.println(item);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void getByPagesCountBetweenTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            Collection<Book> result = BooksBaseOps.getByPagesCountBetween(con, 221, 223);
            for (Book item : result) {
                System.out.println(item);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void getByPagesCountBetweenAndYearBetweenTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            Collection<Book> result = BooksBaseOps.getByPagesCountBetweenAndYearBetween(con, 220, 224, 2002, 2003);
            for (Book item : result) {
                System.out.println(item);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void setPagesForAllBooksExampleOneTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            BooksBaseOps.setPagesForBooksExampleOne(con, 1000);
            Collection<Book> result = BooksBaseOps.getAll(con);
            for (Book item : result) {
                System.out.println(item);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void setPagesForAllBooksExampleTwoTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            BooksBaseOps.setPagesForBooksExampleTwo(con, 1000, 223);
            Collection<Book> result = BooksBaseOps.getAll(con);
            for (Book item : result) {
                System.out.println(item);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void setPagesForAllBooksExampleThreeTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            BooksBaseOps.setPagesForBooksExampleThree(con, 1000, 221, 2004);
            Collection<Book> result = BooksBaseOps.getAll(con);
            for (Book item : result) {
                System.out.println(item);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void setPagesForAllBooksExampleFourTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            BooksBaseOps.setPagesForBooksExampleFour(con, 1000, 200, "dol");
            Collection<Book> result = BooksBaseOps.getAll(con);
            for (Book item : result) {
                System.out.println(item);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void deleteBooksExampleOneTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            BooksBaseOps.deleteBooksExampleOne(con, 222);
            Collection<Book> result = BooksBaseOps.getAll(con);
            for (Book item : result) {
                System.out.println(item);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void deleteBooksExampleTwoTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            BooksBaseOps.deleteBooksExampleTwo(con, "ska");
            Collection<Book> result = BooksBaseOps.getAll(con);
            for (Book item : result) {
                System.out.println(item);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void deleteBooksExampleThreeTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            BooksBaseOps.deleteBooksExampleThree(con, 2, 4);
            Collection<Book> result = BooksBaseOps.getAll(con);
            for (Book item : result) {
                System.out.println(item);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void deleteBooksExampleFourTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            BooksBaseOps.deleteBooksExampleFour(con, "skazka");
            Collection<Book> result = BooksBaseOps.getAll(con);
            for (Book item : result) {
                System.out.println(item);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void deleteBooksExampleFiveTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            BooksBaseOps.deleteBooksExampleFive(con, "kolobok", "dolobok", "skazka");
            Collection<Book> result = BooksBaseOps.getAll(con);
            for (Book item : result) {
                System.out.println(item);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void deleteBooksExampleSixTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            BooksBaseOps.deleteBooksExampleSix(con);
            Collection<Book> result = BooksBaseOps.getAll(con);
            for (Book item : result) {
                System.out.println(item);
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void addNewColumnAndEnumTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            BooksBaseOps.addNewColumn(con, "publishing_house", "varchar(45)");
            BooksBaseOps.addBindingEnum(con);
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void setEnumForAllTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            BooksBaseOps.setEnumForAll(con);
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void setPublishingByIdTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            BooksBaseOps.setPublishingById(con, "ROSMAN", 2,4);
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void changeColumnNameTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            BooksBaseOps.changeColumnName(con, "publishing_house", "pub_house");
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void renameTableTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            BooksBaseOps.renameTable(con, "books", "books_table");
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    @Test
    public void deleteTableTest(){
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            BooksBaseOps.deleteTable(con, "books_table");
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

}
