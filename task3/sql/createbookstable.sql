DROP DATABASE IF EXISTS booksdatabase;
CREATE DATABASE booksdatabase;
USE booksdatabase;

CREATE TABLE books (
  id INT(11) NOT NULL AUTO_INCREMENT,
  title VARCHAR(45) NOT NULL,
  year INT(11) NOT NULL,
  pages INT(11) NOT NULL,
  PRIMARY KEY (id)
)
CHARACTER SET utf8
COLLATE utf8_general_ci;

INSERT INTO books(id, title, year, pages) VALUES (null, 'kolobok', 2000, 220);
INSERT INTO books(id, title, year, pages) VALUES (null, 'wolobok', 2001, 221);
INSERT INTO books(id, title, year, pages) VALUES (null, 'solobok', 2002, 222);
INSERT INTO books(id, title, year, pages) VALUES (null, 'dolobok', 2003, 223);
INSERT INTO books(id, title, year, pages) VALUES (null, 'folobok', 2004, 224);
INSERT INTO books(id, title, year, pages) VALUES (null, 'skazka', 2004, 224);