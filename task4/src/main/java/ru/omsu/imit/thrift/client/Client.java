package ru.omsu.imit.thrift.client;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;

import ru.omsu.imit.thrift.TodoItem;
import ru.omsu.imit.thrift.TodoItemList;
import ru.omsu.imit.thrift.TodoItemService;
import ru.omsu.imit.thrift.TodoListException;

public class Client {

    private static TodoItemService.Client client;

    public static void main(String[] args) {
        try (TTransport transport = new TSocket("localhost", 9090)) {
            transport.open();
            TProtocol protocol = new TBinaryProtocol(transport);
            client = new TodoItemService.Client(protocol);

            addItem("матанализ","Иван","Иванов");
            addItem("программирование","Пётр","Петров");
            addItem("физика","Владимир","Владимирович");
            addItem("философия","Александр","Александрович");

            getAllItems();

            editItem(4, "филология","Александр","Александрович");

            try {
                editItem(5, "психология","Дмитрий","Иванов");
            } catch (TodoListException ex) {
                System.out.println(ex.getErrorString());
            }

            getAllItems();

            deleteItem(4);

            try {
                deleteItem(5);
            } catch (TodoListException ex) {
                System.out.println(ex.getErrorString());
            }

            getAllItems();

        } catch (TTransportException e) {
            e.printStackTrace();
        } catch (TException x) {
            x.printStackTrace();
        }
    }

    private static void addItem(String text, String firstName, String lastName) throws TException {
        TodoItem item = client.add(new TodoItem(0, text, firstName, lastName));
        System.out.println(item.getText() + " " + item.getId());
    }

    private static void getAllItems() throws TException {
        TodoItemList list = client.getAll();
        for (TodoItem item : list.getItems()) {
            System.out.println(item.getText() + " " + item.getId());
        }
    }

    private static void editItem(int id, String text, String firstName, String  lastName) throws TodoListException, TException {
        client.editById(id, text, firstName, lastName);
    }

    private static void deleteItem(int id) throws TodoListException, TException {
        client.deleteById(id);
    }

}