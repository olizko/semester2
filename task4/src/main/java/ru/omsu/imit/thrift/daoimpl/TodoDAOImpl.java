package ru.omsu.imit.thrift.daoimpl;

import java.util.List;

import ru.omsu.imit.thrift.TodoItem;
import ru.omsu.imit.thrift.TodoItemList;
import ru.omsu.imit.thrift.TodoListException;
import ru.omsu.imit.thrift.dao.TodoDAO;
import ru.omsu.imit.thrift.database.DataBaseEmulator;
import ru.omsu.imit.thrift.utils.ErrorCode;


public class TodoDAOImpl implements TodoDAO {

	@Override
	public TodoItem addItem(TodoItem item) {
		DataBaseEmulator.addItem(item);
		return item;
	}
	
	@Override
	public TodoItem getById(int id) throws TodoListException {
		TodoItem item = DataBaseEmulator.getById(id);
		if(item == null)
			throw new TodoListException(ErrorCode.ITEM_NOT_FOUND.toString() + " " + Integer.toString(id));
		return item;
	}
	
	@Override
	public TodoItemList getAll() {
		return DataBaseEmulator.getAll();
	}
	
	
	@Override
	public TodoItem editItem(int id, String newText, String firstName, String lastName) throws TodoListException {
		if(!DataBaseEmulator.editItemById(id, newText))
			throw new TodoListException(ErrorCode.ITEM_NOT_FOUND.toString() + " " + Integer.toString(id));
		return new TodoItem(id, newText, firstName, lastName);
	}
	
	@Override
	public void deleteById(int id) throws TodoListException {
		if(!DataBaseEmulator.deleteById(id))
			throw new TodoListException(ErrorCode.ITEM_NOT_FOUND.toString() + " " + Integer.toString(id));
	}

	@Override
	public TodoItem getByFullName(String firstName, String lastName) throws TodoListException {
		if (firstName == null || lastName == null){
			throw  new TodoListException(ErrorCode.ITEM_NOT_FOUND.toString());
		}
		return DataBaseEmulator.getByFullName(firstName, lastName);
	}

}
