package ru.omsu.imit.thrift.server;

import ru.omsu.imit.thrift.TodoItemService;
import ru.omsu.imit.thrift.serviceimpl.TodoItemServiceImpl;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;

public class TodoItemServer {

	public static void main(String[] args) {

		try {

			TodoItemService.Iface userService = new TodoItemServiceImpl();
			TodoItemService.Processor<TodoItemService.Iface> processor = new TodoItemService.Processor<TodoItemService.Iface>(userService);

			TServerTransport serverTransport = new TServerSocket(9090);
			TServer server = new TSimpleServer(new TServer.Args(serverTransport).processor(processor));
			// TServer server = new TThreadPoolServer(new TThreadPoolServer.Args(serverTransport).processor(processor));

			System.out.println("Starting TodoItem server...");
			server.serve();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}