package ru.omsu.imit.thrift.dao;

import ru.omsu.imit.thrift.TodoItem;
import ru.omsu.imit.thrift.TodoItemList;
import ru.omsu.imit.thrift.TodoListException;

public interface TodoDAO {

	public TodoItem addItem(TodoItem item);
	public TodoItem getById(int id) throws TodoListException;
	public TodoItemList getAll();
	public TodoItem editItem(int id, String newText, String firstName, String lastName) throws TodoListException;
	public void deleteById(int id) throws TodoListException;
	public TodoItem getByFullName(String firstName,String lastName) throws TodoListException;


}